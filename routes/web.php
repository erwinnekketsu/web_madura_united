<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/reset_password', 'Auth\ResetPasswordController@reset_password');
Route::get('/reset-password', 'Auth\ResetPasswordController@reset');
Route::post('/reset-passwords', 'Auth\ResetPasswordController@reset_passwords');

Route::resource('news','NewsController');
Route::get('news/detail/{id}','NewsController@news_detail');
Route::get('news/detail_foto/{id}','NewsController@news_detail_foto');

// TEAM
Route::resource('team','TeamController');

// KLASEMEN
Route::resource('klasemen','KlasemenController');

// KLASEMEN
Route::resource('gallery','GalleryController');

// CLUB
Route::resource('club','ClubController');
Route::get('hubungi','ClubController@hubungi');

//REGISTER
Route::post('register','Auth\RegisterController@register');

//LOGIN
Route::post('login_user', 'Auth\LoginController@login');

//PROFILE
Route::get('profile', 'HomeController@profile');
Route::get('profile_edit', 'HomeController@profile_edit');
Route::post('profile_update', 'HomeController@profile_update');

//ONLINE SHOP
Route::resource('olshop', 'Olshop\HomeController');
Route::get('olshop/{category}/{nama_produk}', 'Olshop\HomeController@detail');

Route::get('order', 'Olshop\HomeController@order');
//CANCEL ORDER
Route::get('cancel_order/{id}', 'Olshop\HomeController@cancel_order');

//Konfirmasi Pembayaran
Route::post('confirm', 'Olshop\HomeController@confirm');
Route::get('confirm', 'Olshop\HomeController@confirm');

Route::get('shop_cart', 'Olshop\HomeController@shop_cart');

Route::get('payment', 'Olshop\HomeController@payment');

//History Order
Route::get('history_order', 'Olshop\HomeController@history_order');
Route::get('history_order_detail/{id}', 'Olshop\HomeController@history_order_detail');

//bukti pembayaran
Route::post('send_payment/{id}', 'Olshop\HomeController@bukti_pembayaran');

//seach
Route::resource('search', 'SearchController');