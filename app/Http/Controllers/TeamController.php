<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->mufc->get('/player-list');
        if ($response->status == 200) {
            $data['data_gk'] = $response->result->results_gk;
            $data['data_df'] = $response->result->results_df;
            $data['data_mf'] = $response->result->results_mf;
            $data['data_af'] = $response->result->results_af;
            // dd($data);
            $data['title'] = $response->result->title;

            return view('team.team', $data);

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
        // return view('team.team');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->mufc->get('/player-bio/'.$id);
        $response_stats = $this->mufc->get('/player-statistik/'.$id);
        $response_all = $this->mufc->get('/player-list-all');
        if ($response->status == 200) {
            $data['data'] = $response->result->results;
            $data['data_all'] = $response_all->result->results;
            $data['data_stats'] = $response_stats->result->results;
            // dd($data['data']->name);
            $data['title'] = $response->result->title;

            return view('team.team_detail   ', $data);

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
        // return view('team.team_detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
