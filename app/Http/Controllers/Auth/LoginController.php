<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected $mufc;
    protected $errorAPI;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Mufc $mufc)
    {
        $this->middleware('guest')->except('logout');
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }

    public function login(Request $request){
        // dd($request->all());
        $response = $this->mufc->post('/login', $request->all());
        // dd($response);
        $status = $response->status;
        if ($response->status == 200) {
            $data = $response->result;
            Session::put('token_type', $data->token_type);
            Session::put('expires_in', $data->expires_in);
            Session::put('access_token', $data->access_token);
            Session::put('refresh_token', $data->refresh_token);
            Session::put('status', $data->status);
            $response_information = $this->mufc->get('/home/setting/personal-information');
            $data_information = $response_information->result->results[0];
            Session::put('name', $data_information->name);
            Session::put('user_email', $data_information->user_email);
            Session::put('user_address', $data_information->user_address);
            Session::put('user_phone', $data_information->user_phone);
            // dd($data_information);
            if (!empty(Session::get('order'))) {
                $datas['order'] = Session::get('order');
                $datas['data_submit'] = Session::get('data_submit');
                return redirect('confirm');
            } else {
                return redirect('home');
            }
        } else {
            return redirect('register');
        }
    }
}
