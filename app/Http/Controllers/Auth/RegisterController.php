<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    protected $mufc;
    protected $errorAPI;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Mufc $mufc)
    {
        $this->middleware('guest');
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request){
        // dd($request->all());
        
        $response = $this->mufc->post('/register_web', $request->all());
        $status = $response->status;
        if($response->status == 200){
            $login['email'] = $request->email;
            $login['password'] = $request->password;
            $response_login = $this->mufc->post('/login', $login);
        // dd($response_login);
            $status = $response_login->status;
            if ($response_login->status == 200) {
                $data = $response_login->result;
                Session::put('token_type', $data->token_type);
                Session::put('expires_in', $data->expires_in);
                Session::put('access_token', $data->access_token);
                Session::put('refresh_token', $data->refresh_token);
                Session::put('status', $data->status);
                $response_login_information = $this->mufc->get('/home/setting/personal-information');
                $data_information = $response_login_information->result->results[0];
                Session::put('name', $data_information->name);
                Session::put('user_email', $data_information->user_email);
                Session::put('user_address', $data_information->user_address);
                Session::put('user_phone', $data_information->user_phone);
            // dd($data_information);
                if (!empty(Session::get('order'))) {
                    $datas['order'] = Session::get('order');
                    $datas['data_submit'] = Session::get('data_submit');
                    return redirect('confirm');
                } else {
                    return redirect('home');
                }
            } else {
                return redirect('register');
            }
        } else {
            return redirect('register');
        }
    }
}
