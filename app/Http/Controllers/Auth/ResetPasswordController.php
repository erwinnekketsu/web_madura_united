<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
        $this->middleware('guest');
    }

    public function reset_password(Request $request){
        $email = $request['email'];
        // dd($request->all());
        $response = $this->mufc->get('/forgot?email='.$email);
        if ($response->status == 200) {
            $data['status'] = $response->result->status;
            $data['message'] = $response->result->message;
            Session::flash('status', $response->result->message);
            return redirect('password/reset');

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    public function reset(Request $request){
        // dd($request->all());
        $data['token'] = $request['token'];
        $data['id'] = $request['id'];
        return response()->view('auth.passwords.reset',$data);
    }

    public function reset_passwords(Request $request)
    {
        $password = $request['password'];
        $password_confirmation = $request['password_confirmation'];
        $token = $request['token'];
        $id = $request['id'];
        // dd($request->all());
        $response = $this->mufc->post('/request-password',['token' => $token ,'id'=>$id,'password'=>$password, 'password_confirmation'=> $password_confirmation]);
        // dd($response);
        if ($response->status == 200) {
            return redirect('/');

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }
}
