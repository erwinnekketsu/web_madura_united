<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session;
use Config;
use Auth;

class HomeController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->mufc->get('/klasemen');
        $response2 = $this->mufc->get('/ads-home');
        $response3 = $this->mufc->get('/list-news-web');
        $response4 = $this->mufc->get('/kompetisi/jadwal-pertandingans');
        $response5 = $this->mufc->get('/kompetisi/hasil-pertandingans');
        $response6 = $this->mufc->get('/popular-news-web');
        $response7 = $this->mufc->get('/slider-home');
        $response8 = $this->mufc->get('/merchand');
        // dd($response5);
        if($response->status == 200){
            $data['data'] = $response->result->result;
            $data['data3'] = $response3->result->results;
            $data['data4'] = $response4->result->result;
            // $data['data5'] = array_reverse(array($response5->result->result));
            $data['data6'] = $response6->result->results;
            $data['data7'] = $response7->result->result;
            $data['data8'] = $response8->result->results;
            $jadwal = $response4->result->result;
            $hasil = array_reverse(array($response5->result->result));
            // dd($hasil[0]);
            // if(!empty($data['data7'])){
            //     return "ada";
            // } else {
            //     return "kosong";
            // }
            foreach ($data['data'] as $item) {
                if ($item->tim == "Madura United") {
                    $data['next_match'] = $item->main + 1;
                }
            }


            $i = 0;
            $data_jadwal = '';
            foreach($jadwal as $item) if ($i++ < 4) {
                if($item->home == "Madura United"){
                    $tim = url('/assets/img/team/').'/'.$item->away.'.png';
                } 
                if($item->away == "Madura United") {
                    $tim = url('/assets/img/team/') . '/' . $item->home . '.png';
                }
                $p = array(
                    'tim' => $tim,
                );
                if ($i == 1) continue;
                $this->_mp->$data_jadwal[$i] = $p;
            }
            $data['jadwal'] = $this->_mp->$data_jadwal;
            // dd($hasil);
            $j = 0;
            $data_hasil = '';
            foreach($hasil[0] as $item2) if ($j++ < 3) {
                if($item2->home == "Madura United"){
                    $tim2 = url('/assets/img/team/') . '/' . $item2->away . '.png';
                } 
                if($item2->away == "Madura United") {
                    $tim2 = url('/assets/img/team/') . '/' . $item2->home . '.png';
                }
                $p2 = array(
                    'tim' => $tim2,
                );
                $this->_mps->$data_hasil[$j] = $p2;
            }
            $data['hasil'] = $this->_mps->$data_hasil;
            if(!empty($response2->result->result)){
                $datas['data2'] = $response2->result->result;
            }
            $data['title'] = $response->result->title; 
            if(!empty($datas['data2'])){
                foreach($datas['data2'] as $item){
                    if($item->type == 2 && $item->position == 1){
                        $data['gambar1'] = $item->path;
                    }
                    if($item->type == 1 && $item->position == 1){
                        $data['gambar2'] = $item->path;
                    }
                    if($item->type == 1 && $item->position == 2){
                        $data['gambar3'] = $item->path;
                    }
                    if($item->type == 1 && $item->position == 3){
                        $data['gambar4'] = $item->path;
                    }
                }
            }
            // dd($data);   
            return view('welcome', $data);
        } else {
            // echo $this->errorAPI;
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
        // return view('welcome');
    }

    public function profile()
    {
        if (!empty(Session::get('access_token'))) {
            $response = $this->mufc->get('/home/setting/personal-information');
            if ($response->status == 200) {
                $data['profile'] = $response->result->results[0];
                // dd($data['profile']);
                return view('auth.profile', $data);
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }

    public function profile_edit()
    {
        if (!empty(Session::get('access_token'))) {
            $response = $this->mufc->get('/home/setting/personal-information');
            if ($response->status == 200) {
                $data['profile'] = $response->result->results[0];
                // dd($data['profile']);
                return view('auth.profile_edit', $data);
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }

    public function profile_update(Request $request)
    {
        if (!empty(Session::get('access_token'))) {
            $data['name'] = $request->name;
            $data['address'] = $request->address;
            $data['phone_number'] = $request->phone_number;
            // dd($data);
            $response = $this->mufc->post('/home/setting/personal-information',$data);
            // dd($response);
            if ($response->status == 200) {
                return redirect('profile');
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }
}
