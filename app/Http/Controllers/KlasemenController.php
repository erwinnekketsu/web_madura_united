<?php

namespace App\Http\Controllers;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class KlasemenController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->mufc->get('/kompetisi/jadwal-pertandingans');
        $response3 = $this->mufc->get('/kompetisi/hasil-pertandingans');
        $response2 = $this->mufc->get('/klasemen');
        if ($response->status == 200) {
            $data['data'] = $response->result->result;
            $data['data2'] = $response2->result->result;
            $data['data3'] = array_reverse(array($response3->result->result));
            // dd($data['data']);
            foreach($data['data2'] as $item){
                if($item->tim == "Madura United"){
                    $data['next_match'] = $item->main + 1;
                }
            }
            $data['title'] = $response->result->title;

            return view('klasemen.index', $data);

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
