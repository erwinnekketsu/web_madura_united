<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->mufc->get('/gallery');
        if ($response->status == 200) {
            $data['data'] = $response->result->results;
            // dd($data['data']);
            $data['title'] = $response->result->title;

            return view('gallery.index', $data);

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->mufc->get('/gallery'.'/'.$id);
        // dd($response);
        if ($response->status == 200) {
            $data['data'] = $response->result->results;
            $data['title'] = $response->result->title;
            if (!empty($data['data'][0])) {
                # code...
                $data['title_gallery'] = $data['data'][0]->title_gallery;
            } else {
                $data['error'] = "Gallery masih kosong";
                return response()->view('layouts.404', $data);
            }
            return view('gallery.detail', $data);

        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
