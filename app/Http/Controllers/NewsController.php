<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->mufc->get('/list-news-web');
        $response2 = $this->mufc->get('/klasemen');
        $response3 = $this->mufc->get('/kompetisi/jadwal-pertandingans');
        $response4 = $this->mufc->get('/popular-news-web');
        // dd($response);
        if ($response->status == 200) {
            $data['data'] = $response->result->results;
            $data['data2'] = $response2->result->result;
            $data['data3'] = $response3->result->result;
            $data['data4'] = $response4->result->results;
            // dd($data['data2']);
            foreach ($data['data2'] as $item) {
                if ($item->tim == "Madura United") {
                    $data['next_match'] = $item->main + 1;
                }
            }
            return view('news', $data);
        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function news_detail($id)
    {
        $response = $this->mufc->get('/klasemen');
        $response2 = $this->mufc->get('/ads-home');
        $response3 = $this->mufc->get('/kompetisi/jadwal-pertandingans');
        $response4 = $this->mufc->get('/news/'.$id);
        $response5 = $this->mufc->get('/list-news-web');
        $response6 = $this->mufc->get('/popular-news-web');
        // dd($response2);
        if ($response2->status == 200) {
            if (!empty($response2->result->result)) {
                $datas['data2'] = $response2->result->result;
            }
            $data['data'] = $response->result->result;
            $data['data3'] = $response3->result->result;
            $data['data4'] = $response4->result->results;
            $data['data5'] = $response5->result->results;
            $data['data6'] = $response6->result->results;
            // dd($data['data6']);
            $i = 0;
            foreach($data['data5'] as $item_news){
                if($i == 0){
                    if($item_news->id == $data['data4']->id){
                        continue;
                    } else {
                        $data['next_news'] = $item_news->slug;
                        $data['prev_news'] = end($data['data5'])->slug;
                        break;
                    }
                } else {
                    
                }
                $i++;
            }
            // dd($data['prev_news']);

            if (!empty($datas['data2'])) {
                foreach ($datas['data2'] as $item) {
                    if ($item->type == 1 && $item->position == 4) {
                        $data['iklan_4'] = $item->path;
                    }
                    if ($item->type == 1 && $item->position == 5) {
                        $data['iklan_5'] = $item->path;
                    }
                }
            }
            foreach ($data['data'] as $item) {
                if ($item->tim == "Madura United") {
                    $data['next_match'] = $item->main + 1;
                }
            }
            return view('news.detail-news', $data);
        } else {
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    public function news_detail_foto($id){
        return view('news.detail-news-foto');   
    }
}
