<?php

namespace App\Http\Controllers\Olshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\Mufc;
use GuzzleHttp\Client;
use Session;
use Config;
use Auth;

class HomeController extends Controller
{
    protected $mufc;
    protected $errorAPI;

    public function __construct(Mufc $mufc)
    {
        // $this->middleware(function ($request, $next) {
        //     if (empty(Session::get('access_token'))) {
        //         return redirect('login');
        //     }else{

        //         return $next($request);
        //     }

        // });
        $this->mufc = $mufc;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Session::all());
        $response = $this->mufc->get('/merchand');
        if ($response->status == 200) {
            $data['data'] = $response->result->results;
            return view('olshop.home',$data);
        } else {
            // echo $this->errorAPI;
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response2 = $this->mufc->get('/merchand');
        if ($response2->status == 200) {
            $data['data'] = $response2->result->results;
            // dd($data['data']);
            return view('olshop.category', $data);
        } else {
            // echo $this->errorAPI;
            $data['error'] = $response2->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($category,$nama_produk)
    {
        $response = $this->mufc->get('/merchand/'.$nama_produk);
        $response2 = $this->mufc->get('/merchand');
        if ($response->status == 200) {
            $data['data'] = $response->result->results[0];
            $data['data2'] = $response2->result->results;
            // dd($data['data']);
            return view('olshop.detail', $data);
        } else {
            // echo $this->errorAPI;
            $data['error'] = $response->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    public function order(Request $request){
        // return $request->all();
        if (!empty(Session::get('order'))) {
            $order = array_column(Session::get('order'), 'id_merch');
            $found_key = json_encode(array_search($request['id_merch'], $order));
        }

        if (empty(Session::get('order'))) {
            $data['order'] = array(array(
                'id_merch'=> $request['id_merch'],
                'nama_merch'=> $request['nama_merch'],
                'harga'=> $request['harga'],
                'image'=> $request['image'],
                'stok'=> $request['stok'],
            ));
            $request->session()->put('order', $data['order']);
        } else if($found_key === "false"){
            $data['order'] = array(
                'id_merch' => $request['id_merch'],
                'nama_merch' => $request['nama_merch'],
                'harga' => $request['harga'],
                'image' => $request['image'],
                'stok' => $request['stok'],
            );
            $request->session()->push('order', $data['order']);
        } else {
            return redirect('shop_cart');
        }
        return redirect('shop_cart');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function shop_cart(Request $request)
    {
        // $colors = array_column(Session::get('order'), 'id_merch');
        // $found_key = array_search('MRC3', $colors);
        // dd($found_key);
        // $key = array_search('MRC1', array_column(Session::get('order'), 'id_merch') );
        // $request->session()->flush();
        $response2 = $this->mufc->get('/merchand');
        if ($response2->status == 200) {
            if (!empty(Session::get('order'))) {
                $data['order'] = Session::get('order');
            } else {
                $data['order'] = '';
            }
            $data['data2'] = $response2->result->results;
            // dd($data['data']);
            return view('olshop.shop_cart',$data);
        } else {
            // echo $this->errorAPI;
            $data['error'] = $response2->status;
            // dd($data);
            return response()->view('layouts.404', $data);
        }
    }

    public function cancel_order(Request $request,$id){
        // return $id;
        // $sessionKey = Config::get('session.order');
        // dd($sessionKey);
        $order = array_column(Session::get('order'), 'id_merch');
        // dd($order);
        $found_order = json_encode(array_search($id, $order));
        // dd($found_order);
        if ($found_order === "false") {
            return redirect('shop_cart');
        } else {
            // Session::pull('order');
            $ingredients = Session::get('order');
            foreach ($ingredients as $key => $value) {
                // dd($key);
                if (isset($value['id_merch']) && $id == $value['id_merch'])
                    unset($ingredients[$key]);
            }
            $request->session()->put('order', $ingredients);
            // dd($ingredients);
            return redirect('shop_cart');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        if (!empty(Session::get('order'))) {
            $data['order'] = Session::get('order');
            $data['jumlah'] = $request->jumlah;
        } else {
            $data['order'] = '';
            $data['jumlah'] = '';
        }
        return view('olshop.payment',$data);
    }

    public function confirm(Request $request){
        // return Session::get('data_submit');
        if (!empty(Session::get('access_token'))) {
            if (!empty(Session::get('order'))) {
                $data['order'] = Session::get('order');
                $data['data_submit'] = Session::get('data_submit');
                if(!empty($data['data_submit'])){
                    $data['data_submit'] = Session::get('data_submit');
                } else {
                    $data['data_submit'] = $request->all();
                    // dd('test');
                }
            } else {
                $data['order'] = '';
                $data['data_submit'] = '';
            }
            // return $data;
            $response = $this->mufc->post('/order_merchand',$data);
            // dd($response);
            if ($response->status == 200) {
                $data['message'] = $response->result->message;
                // dd($data['data']);
                $request->session()->forget('order');
                return redirect('olshop')->with('message', $data['message']);
            } else {
                // echo $this->errorAPI;
                $data['error'] = $response->status;
                // dd($data);
                return response()->view('layouts.404', $data);
            }
            // return redirect('olshop')->with('message', 'Profile updated!');
            // return Session::all();
            // dd(Session::all());
        } else {
            $data['data_submit'] = $request->all();
            // return $data;
            $request->session()->put('data_submit', $data['data_submit']);
            return redirect('login');
        }
        
    }

    public function history_order(Request $request){
        if (!empty(Session::get('access_token'))) {
            $email = Session::get('user_email');
            $response = $this->mufc->post('/history_order', ['email'=>$email]); 
            if ($response->status == 200) {
                $data['history'] = $response->result;
                    // dd($data['history']);
                return view('olshop.history',$data);
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }

    public function history_order_detail(Request $request,$id){
        if (!empty(Session::get('access_token'))) {
            $email = Session::get('user_email');
            $response = $this->mufc->post('/detail_history_order/'.$id, ['email'=>$email]); 
            if ($response->status == 200) {
                $data['history'] = $response->result->results;
                // dd($data['history']);
                return view('olshop.history_detail',$data);
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }

    public function bukti_pembayaran(Request $request, $id){
        if (!empty(Session::get('access_token'))) {
            $file = $request['bukti_pembayaran'];
            $oriname = $file->getClientOriginalName();
            $path = $file->getPathname();
            // dd($path);
            $email = Session::get('user_email');
            $response = $this->mufc->post('/bukti_pembayaran/' . $id, [
                // ['bukti_pembayaran' => $file]
                    'multipart' => [
                        'name' => 'bukti_pembayaran',
                        'contents' => fopen($file->getPathname(), 'r'),
                        'filename' => $file->getClientOriginalName()
                    ]
                ]
            );
            dd($response);
            if ($response->status == 200) {
                $data['history'] = $response->result;
                // dd($data['history']);
                return view('olshop.history_detail', $data);
            } else {
                    // echo $this->errorAPI;
                $data['error'] = $response->status;
                    // dd($data);
                return response()->view('layouts.404', $data);
            }
        } else {
            return redirect('login');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
