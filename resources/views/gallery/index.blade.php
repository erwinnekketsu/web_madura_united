@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/gallery.css">
<div class="main-container">
     <div class="container" style="    background: #fff;">
        <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="gallery-title"></h1>
            <div align="center" class="nav-gallery">
                <button class="btn btn-default filter-button" data-filter="all">All</button>
                <button class="btn btn-default filter-button" data-filter="foto">Foto</button>
                <button class="btn btn-default filter-button" data-filter="video">Video</button>
            </div>
        </div>

        <br/>
            @foreach($data as $item)
                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter foto">
                    <a href="{{url('')}}/gallery/{{$item->id}}"><img src="{{$item->thumbnail}}" class="img-responsive"></a>
                </div>
            @endforeach
        </div>
    </div>
</div>
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')
<script>
    $(document).ready(function(){

        $(".filter-button").click(function(){
            var value = $(this).attr('data-filter');
            
            if(value == "all")
            {
                //$('.filter').removeClass('hidden');
                $('.filter').show('1000');
            }
            else
            {
    //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
    //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');
                
            }
        });
        
        if ($(".filter-button").removeClass("active")) {
    $(this).removeClass("active");
    }
    $(this).addClass("active");

    });
</script>