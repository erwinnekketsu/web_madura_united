@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/team.css">
<style>
    @media (max-width: 576px) and (min-width: 320px){
        #mobile-nav-toggle {
            top: 6% !important;
        }
    }
</style>
<div class="team">
    <div class="team-content">
        <div class="team-content-header">
            <div class="selected-item">
                <p>You Team  : <span>Pemain Senior</span></p>
            </div>
            
            <div class="dropdown">
                <span class="selLabel">Select Team</span>
                <input type="hidden" name="cd-dropdown">
                <ul class="dropdown-list">
                <li data-value="1">
                    <span>Senior</span>
                </li>
                <li data-value="2">
                    <span>U-19</span>
                </li>
                <li data-value="3">
                    <span>U-17</span>
                </li>
                </ul>
            </div>
        </div>
        <div class="team-content-main">
            <section class="portfolio-container" class="padding-60">
                <div class="container">
                    <div class="row">
                        <div class="text-center">
                            <div class="toolbar mb2 mt2 padding-b-25">
                                <button class="btn fil-cat active" href="" data-rel="all">All</button>
                                <button class="btn fil-cat" data-rel="goalkeeper">Goal Keeper</button>
                                <button class="btn fil-cat" data-rel="defender">Defender</button>
                                <button class="btn fil-cat" data-rel="midfielder">Midfielder</button>
                                <button class="btn fil-cat" data-rel="forward">Forward</button>
                            </div> 
                        </div> 
            
                        <div style="clear:both;"></div>   
                        <div class="portfolio">
                            <div class="tile scale-anm goalkeeper all">
                                <div class="tile-header-goalkeeper goalkeeper all">
                                    <p>Goal Keeper</p>
                                </div>
                                <div class="tile-content goalkeeper all">
                                    @foreach($data_gk as $item)
                                    <a href="{{url('/team/')}}/{{$item->id_player}}">
                                        <div class="players goalkeeper all">
                                            <div class="players-photo goalkeeper all">
                                                <img src="{{$item->image}}" alt="">
                                            </div>
                                            <div class="players-info goalkeeper all">
                                                <div class="players-info-name goalkeeper all">
                                                    <h4>{{strtoupper($item->name)}}</h4>
                                                    <h5>{{strtoupper($item->position)}}</h5>
                                                </div>
                                                <div class="players-info-number goalkeeper all">
                                                    <h1>{{$item->jersey_number}}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tile scale-anm defender all">
                                <div class="tile-header-defender defender all">
                                    <p>Defender</p>
                                </div>
                                <div class="tile-content-defender defender all">
                                    @foreach($data_df as $item)
                                    <a href="{{url('/team/')}}/{{$item->id_player}}">
                                        <div class="players defender all">
                                            <div class="players-photo defender all">
                                                <img src="{{$item->image}}" alt="">
                                            </div>
                                            <div class="players-info defender all">
                                                <div class="players-info-name defender all">
                                                    <h4>{{strtoupper($item->name)}}</h4>
                                                    <h5>{{strtoupper($item->position)}}</h5>
                                                </div>
                                                <div class="players-info-number defender all">
                                                    <h1>{{$item->jersey_number}}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tile scale-anm midfielder all">
                                <div class="tile-header-midfielder midfielder all">
                                    <p>midfielder</p>
                                </div>
                                <div class="tile-content-midfielder midfielder all">
                                    @foreach($data_mf as $item)
                                    <a href="{{url('/team/')}}/{{$item->id_player}}">
                                        <div class="players midfielder all">
                                            <div class="players-photo midfielder all">
                                                <img src="{{$item->image}}" alt="">
                                            </div>
                                            <div class="players-info midfielder all">
                                                <div class="players-info-name midfielder all">
                                                    <h4>{{strtoupper($item->name)}}</h4>
                                                    <h5>{{strtoupper($item->position)}}</h5>
                                                </div>
                                                <div class="players-info-number midfielder all">
                                                    <h1>{{$item->jersey_number}}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tile scale-anm forward all">
                                <div class="tile-header-forward forward all">
                                    <p>forward</p>
                                </div>
                                <div class="tile-content-forward forward all">
                                    @foreach($data_af as $item)
                                    <a href="{{url('/team/')}}/{{$item->id_player}}">
                                        <div class="players forward all">
                                            <div class="players-photo forward all">
                                                <img src="{{$item->image}}" alt="">
                                            </div>
                                            <div class="players-info forward all">
                                                <div class="players-info-name forward all">
                                                    <h4>{{strtoupper($item->name)}}</h4>
                                                    <h5>{{strtoupper($item->position)}}</h5>
                                                </div>
                                                <div class="players-info-number forward all">
                                                    <h1>{{$item->jersey_number}}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>   
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<script src="{{url('')}}/assets/js/vendor/jquery-2.2.4.min.js"></script>
<script>
    $(document).ready(function() {
  
    $(".selLabel").click(function () {
    $('.dropdown').toggleClass('active');
    });

    $(".dropdown-list li").click(function() {
    $('.selLabel').text($(this).text());
    $('.dropdown').removeClass('active');
    $('.selected-item p span').text($('.selLabel').text());
    });

    });
</script>
<script>
    $(function() {
		var selectedClass = "";
		$(".fil-cat").click(function(){ 
		selectedClass = $(this).attr("data-rel"); 
     $(".portfolio").fadeTo(100, 0.1);
		$(".portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
    setTimeout(function() {
      $("."+selectedClass).fadeIn().addClass('scale-anm');
      $(".portfolio").fadeTo(300, 1);
    }, 300); 
		
	});
});

</script>
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')