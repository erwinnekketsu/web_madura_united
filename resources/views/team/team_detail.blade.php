@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/team.css">
<style>
    .players {
        width: 10%;
        margin-right: 0%;
    }
    .players-info-number h1 {
        margin-left: 0%;
        font-size: xx-large;
        color: #fff;
    }
    .players-info-name .h4{
        /* word-wrap: break-word; */
    }
</style>
<div class="team-detail">
    <div class="main-detail-team-foto-content">
        <div class="content-detail-team-foto">
            <div class="content-detail-team-left">
                <div class="content-detail-team-left-top">
                    <h1>{{$data->name}}</h1>
                    <hr>
                    <div class="footer-medsoss"><a href="{{$data->fb_link}}"><i class="fa fa-facebook"></i></a></div>
                    <div class="footer-medsoss"><a href="{{$data->ig_link}}"><i class="fa fa-instagram"></i></a></div>
                    <div class="footer-medsoss"><a href="{{$data->tw_link}}"><i class="fa fa-twitter"></i></a></div>
                </div>
                <div class="content-detail-team-left-bottom">
                    <h3>Negara : {{$data->nationality}}</h3>
                    <h3>Tanggal Lahir : {{$data->birth_date}}</h3>
                    <h3>Tinggi : {{$data->height}} Cm</h3>
                    <h3>Berat : {{$data->weight}} Kg</h3>
                    <h3>Bergabung : {{$data->join_madura}}</h3>
                    <h3>Klub Sebelumnya : {{$data->team_before}}</h3>
                    <h3>Status : {{$data->status}}</h3>
                </div>
            </div>
            <div class="content-detail-team-center">
                <img src="{{$data->image_body}}" alt="">
            </div>
            <div class="content-detail-team-right">
                <div class="content-detail-team-right-top">
                    <h1>{{$data->jersey_number}}</h1>
                    <h1>{{$data->position}}</h1>
                </div>
                <div class="content-detail-team-right-bottom">
                    <img src="{{url('/assets/img/')}}/red_card.png" alt=""> : {{$data_stats->red_card}}
                    <img src="{{url('/assets/img/')}}/yellow_card.png" alt=""> : {{$data_stats->yellow_card}}
                    <br>
                    <br>
                    <br>
                    <h1>{{$data_stats->game}} <br> Game</h1>
                    <h1>{{$data_stats->goal}} <br> Goal</h1>
                    <h1>{{$data_stats->assist}} <br> Assist</h1>
                </div>
            </div>
        </div>
        <div class="content-list-team-foto">
            @foreach($data_all as $item)
            <a href="{{url('/team/')}}/{{$item->id_player}}">
                <div class="players goalkeeper all">
                    <div class="players-photo goalkeeper all">
                        <img src="{{$item->image}}" alt="">
                    </div>
                    <div class="players-info goalkeeper all">
                        <div class="players-info-name goalkeeper all">
                            <h4>{{strtoupper($item->name)}}</h4>
                            <h5>{{strtoupper($item->position)}}</h5>
                        </div>
                        <div class="players-info-number goalkeeper all">
                            <h1>{{$item->jersey_number}}</h1>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')