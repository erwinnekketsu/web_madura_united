<img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif" id="loading-indicator" style="display:none" />
<style>
    .easyPaginateNav {
        margin-left: 1.5%;
    }
    .easyPaginateNav a {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #DA1D1A;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .easyPaginateNav a.current {
        font-weight:bold;
        text-decoration:underline;
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #DA1D1A;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
</style>
<div class="main-news">
    <div class="news-content">
        <div class="news-content-left">
            <div class="news-content-top">
                <div id="demo2" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo2" data-slide-to="0" class="active"></li>
                        <li data-target="#demo2" data-slide-to="1"></li>
                        <li data-target="#demo2" data-slide-to="2"></li>
                    </ul>
                    
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @php $i = 0 @endphp
                        @foreach($data as $item)
                        <div class="carousel-item @if($i == 0) active @endif">
                            <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                                <img src="{{$item->image_show_fp}}"  width="100%" height="100%">
                                <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 40%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 100%;height: 36%;">
                                <div class="slider-text">
                                    <h5>Berita</h5>
                                    <h1>{{$item->title}}</h1>
                                </div>
                            </a>
                        </div>
                        @if($i == 2) @php break; @endphp @endif
                        @php $i++; @endphp
                        @endforeach
                    </div>
                    
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo2" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo2" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
            <div class="news-content-bottom">
                <div class="news-content-bottom-left">
                    <div id="demo3" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo3" data-slide-to="0" class="active"></li>
                        <li data-target="#demo3" data-slide-to="1"></li>
                        <li data-target="#demo3" data-slide-to="2"></li>
                    </ul>
                    
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @php $i = 0 @endphp
                        @php $datas = array_reverse($data) @endphp
                        @foreach($datas as $item)
                        <div class="carousel-item @if($i == 0) active @endif">
                            <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                                <img src="{{$item->image_show_fp}}"  height="100%">
                                <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 10%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 101%;height: 45%;">
                                <div class="slider-text">
                                    <h5>Berita</h5>
                                    <h1 style="color: #fff;font-size: 15pt;">{{$item->title}}</h1>
                                </div>
                            </a>
                        </div>
                        @if($i == 2) @php break; @endphp @endif
                        @php $i++; @endphp
                        @endforeach
                    </div>
                    
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo3" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo3" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
                </div>
                <div class="news-content-bottom-right">
                     <div id="demo4" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo4" data-slide-to="0" class="active"></li>
                        <li data-target="#demo4" data-slide-to="1"></li>
                        <li data-target="#demo4" data-slide-to="2"></li>
                    </ul>
                    
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @for ($a = 1; $a < 4; $a++)
                            <div class="carousel-item @if($a == 1) active @endif">
                            <a href="{{url('')}}/news/detail/{{$data[$a]->slug}}"  >
                                <img src="{{$data[$a]->image_show_fp}}" height="100%">
                                <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 10%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 101%;height: 45%;">
                                <div class="slider-text">
                                    <h5>Berita</h5>
                                    <h1 style="color: #fff;font-size: 15pt;">{{$data[$a]->title}}</h1>
                                </div>
                            </a>
                        </div>
                        @endfor
                    </div>
                    
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo4" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo4" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
                </div>
            </div>
        </div>
        <div class="news-content-right">
            <div class="news-content-right-top">
                <div class="news-content-right-top-header">
                    <div class="news-content-right-top-header-left">
                        <h3>Berita Terpopuler</h3>
                        <hr>
                    </div>
                    <div class="news-content-right-top-header-right">
                        
                    </div>
                    <hr>
                </div>
                <div class="news-content-right-top-content">
                    @foreach($data4 as $item)
                    <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                    <div class="row news-content-right-top-content-row" >
                        <div class="col-sm-12">
                            <div class="photo">
                                <img src="{{$item->image_show_fp}}" class="img-responsive" alt="a" />
                            </div>
                            <div class="info">
                                <div class="row">
                                    <div class="tgl col-md-12">
                                        <h5>{{$item->stardate}}</h5>
                                    </div>
                                    <div class="title col-md-12">
                                        <h5>{{$item->title}}</h5>
                                    </div>
                                    <div class="resume col-md-12">
                                        {!!$item->summary!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                    @endforeach
                </div>
                <div class="news-content-right-top-footer">
                    <h5><a href="#">Baca Berita Lainnya</a></h5>
                </div>
            </div>
            <div class="news-content-right-bottom">
                <div class="news-content-next-match">
                    @foreach($data3 as $item)
                    <div class="next-match-content-left">
                        <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="">
                        <h3>{{$item->home}}</h3>
                    </div>
                    <div class="next-match-content-center">
                        <h3>NEXT MATCH</h3>
                        <h4>Gojek Liga 1, Pertandingan Ke {{$next_match}}</h4>
                        <h4 class="next-match-date">{{$item->date}}</h4>
                        <h4>{{$item->schedule}} WIB</h4>
                    </div>
                    <div class="next-match-content-right">
                        <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt="">
                        <h3>{{$item->away}}</h3>
                    </div>
                    @php break @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="all-news">
    <div class="container">
        <div class="col-md-12">
			<div class="row">
                <div class="col-xs-12 " style="width: 100%;">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="true">All News</a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                            <div id="easyPaginate">
                                @php $i = 0 @endphp
                                @foreach($data as $item)
                                <div class="row news-right-content-row" id="paginate">
                                    <div class="col-sm-12">
                                        <div class="col-item">
                                                <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                                                <div class="photo">
                                                    <img src="{{$item->image_show_fp}}" class="img-responsive" alt="a" />
                                                </div>
                                                <div class="info">
                                                    <div class="row">
                                                        <div class="price col-md-12">
                                                            <h5>{{$item->stardate}}</h5>
                                                        </div>
                                                        <div class="title col-md-12">
                                                            <h5>{{$item->title}}</h5>
                                                        </div>
                                                        <div class="resume col-md-12">
                                                            {!!$item->summary!!}
                                                        </div>
                                                    </div>
                                                </div>
                                                </a>
                                            </div>
                                        </div>
                                </div>
                                @php $i++; @endphp
                                @endforeach
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
		</div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="{{url('')}}/js/jquery.easyPaginate.js"></script>
<script>
    $('#easyPaginate').easyPaginate({
        paginateElement: 'div#paginate',
        elementsPerPage: 9,
        effect: 'default'
    });
</script>