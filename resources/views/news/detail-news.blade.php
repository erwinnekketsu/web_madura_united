@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/main-news.css">
<style type="text/css">
    .title-background{
        position: absolute; left: 50%; transform: translate(-50%, -36%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 85%;height: 6%;
    }
</style>
<div class="detail-news-image">
    <div class="detail-news-image-image">
        <div class="photo">
            <img src="{{$data4->image_show_fp}}" class="img-responsive" alt="a" />
            <!-- <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 21%; left: 50%; transform: translate(-50%, -50%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 85%;height: 5%;"> -->
        </div>
        <div class="info">
            <div class="">
                <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" class="title-background">
                <div class="title col-md-12">
                    <h5>{{$data4->title}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="detail-news-content">
    <div class="main-detail-news-content">
        <div class="main-detail-news-content-left">
            <div class="main-detail-news-content-left-head">
                <div class="header-content-detail-news-left">
                    <h5>{{date('d/m/Y', strtotime($data4->stardate))}}</h5>
                </div>
                <div class="header-content-detail-news-right">
                </div>
            </div>
            <div class="main-detail-news-content-left-content">
                {!!$data4->body!!}
            </div>
            <div class="main-detail-news-content-left-footer">
                <div class="footer-content-detail-news-left">
                    <a href="#"><i class="fa fa-arrow-left"></i> Sebelumnya</a>
                </div>
                <div class="footer-content-detail-news-center">
                    <h5>Bagikan:</h5>
                    <div class="share-socmed">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank"><i class="fa fa-facebook"></i></a>
                    </div>
                    <!-- <div class="share-socmed">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div> -->
                    <div class="share-socmed">
                        <a href="https://twitter.com/intent/tweet?text=Share Madura United Fc News&amp;url={{url()->current()}}" target="_blank"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
                <div class="footer-content-detail-news-right">
                    <h5><a href="#">Selanjutnya <i class="fa fa-arrow-right"></i></a></h5>
                </div>
            </div>
         </div>
        <div class="main-detail-news-content-right">
            <div class="news-content-right-top">
                <div class="news-content-right-top-header">
                    <div class="news-content-right-top-header-left">
                        <h3>Berita Terpopuler</h3>
                        <hr>
                    </div>
                    <div class="news-content-right-top-header-right">
                        
                    </div>
                    <hr>
                </div>
                <div class="news-content-right-top-content">
                    @foreach($data6 as $item)
                    <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                    <div class="row news-content-right-top-content-row" >
                        <div class="col-sm-12">
                            <div class="photo">
                                <img src="{{$item->image_show_fp}}" class="img-responsive" alt="a" />
                            </div>
                            <div class="info">
                                <div class="row">
                                    <div class="tgl col-md-12">
                                        <h5>{{date('d/m/Y', strtotime($item->stardate))}}</h5>
                                    </div>
                                    <div class="title col-md-12">
                                        <h5>{{$item->title}}</h5>
                                    </div>
                                    <div class="resume col-md-12">
                                        {!!$item->summary!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                    @endforeach
                </div>
                <div class="news-content-right-top-footer">
                    <h5><a href="#">Baca Berita Lainnya</a></h5>
                </div>
            </div>
            <div class="news-content-right-bottom">
                <div class="news-content-right-banner1">
                    <div class="news-content-next-match">
                        @foreach($data3 as $item)
                        <div class="next-match-content-left">
                            <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="">
                            <h3>{{$item->home}}</h3>
                        </div>
                        <div class="next-match-content-center">
                            <h3>NEXT MATCH</h3>
                            <h4>Gojek Liga 1, Pertandingan Ke {{$next_match}}</h4>
                            <h4 class="next-match-date">{{$item->date}}</h4>
                            <h4>{{$item->schedule}} WIB</h4>
                        </div>
                        <div class="next-match-content-right">
                            <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt="">
                            <h3>{{$item->away}}</h3>
                        </div>
                        @php break @endphp
                        @endforeach
                    </div>
                </div>
                <!-- <div class="news-content-right-banner2">
                    <div class="news-content-next-match-iklan1">
                        @if(!empty($iklan_4))
                        <img src="{{$iklan_4}}" alt="" title="" style="height:100%;width:100%;"/>
                        @else
                        <img src="{{url('')}}/assets/img/Kode-Promo-Lion-Air-Airpaz.png" alt="" style="height:100%;width:100%;">
                        @endif
                    </div>
                </div> -->
                <div class="news-content-right-banner3">
                    <div class="news-content-next-match-iklan2">
                        @if(!empty($iklan_5))
                        <img src="{{$iklan_5}}" alt="" title="" style="height:100%;width:100%;"/>
                        @else
                        <img src="{{url('')}}/assets/img/maxresdefault.png" alt="" style="height:100%;width:100%;">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')