@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/main-news.css">
<link rel="stylesheet" href="{{url('')}}/assets/css/slide-main.css">
<link rel="stylesheet" href="{{url('')}}/assets/css/slide-left.css">
<link rel="stylesheet" href="{{url('')}}/assets/css/slide-right.css">

@include('news.main-news')
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')
