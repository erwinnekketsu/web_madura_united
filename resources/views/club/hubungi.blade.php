@include('layouts.head')
@include('layouts.header')
<style>

    @media (max-width: 360px){

        #mobile-nav-toggle {
            display: inline;
            top: 12% !important;
        }
        #logo-header {
            width: 50%;
            padding-left: 4%;
        }
    }
    @media screen and (max-width: 568px) and (min-width: 320px)
    {
        .content-club p {
            width: 100%;
            font-size: 14px;
            color: #000;
        }
    }
</style>
<div class="main-club">
    <div class="club">
        <div class="header-club">
            <div class="header-club-logo">
                <img src="{{url('')}}/assets/img/team/Madura United.png" alt="">
            </div>
            <div class="header-club-title">
                <h1>Hubungi Kami</h1>
            </div>
        </div>
        <div class="content-club">
            <h1 style="font-size: x-large;font-weight: bold;">Distro Pamekasan</h1>
            <h4 style="font-size: large;margin-bottom:1%">Jl. Raya Panglegur No. 10 Km 1
                (Utara Terminal Baru)</h4>
            <h1 style="font-size: x-large;font-weight: bold;">Distro Sumenep</h1>
            <h4 style="font-size: large;margin-bottom:1%">Jl Raya Trunojoyo No. 110
	            (SPBU Gedungan)</h4>
            <h1 style="font-size: x-large;font-weight: bold;">Distro Bangkalan</h1>
            <h4 style="font-size: large;margin-bottom:1%">Jl. Soekarno Hatta No. 4A 
	            (Depan Stadion Gelora Bangkalan)</h4>
            <h1 style="font-size: x-large;font-weight: bold;">Distro Sampang</h1>
            <h4 style="font-size: large;margin-bottom:1%">Jl. Syamsul Arifin No. 20
                (Depan Gang SMPN 3 Sampang)</h4>
            <p>
                Nomor kontak:  0812 333 77 432
            </p>
        </div>
    </div>
</div>

@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')