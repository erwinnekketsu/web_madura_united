@include('layouts.head')
@include('layouts.header')
<style>
    @media (max-width: 360px){

        #mobile-nav-toggle {
            display: inline;
            top: 12% !important;
        }
        #logo-header {
            width: 50%;
            padding-left: 4%;
        }
    }
  
</style>
<div class="main-club">
    <div class="club">
        <div class="header-club">
            <div class="header-club-logo">
                <img src="{{url('')}}/assets/img/team/Madura United.png" alt="">
            </div>
            <div class="header-club-title">
                <h1>LASKAR SAPEH KERABB</h1>
                <h3>"Salam Settong Dhere"</h3>
            </div>
        </div>
        <div class="content-club">
            <p>Madura United FC merupakan klub sepak bola asal Indonesia yang berbasis di Pamekasan, Madura. Klub ini sebelumnya bernama Persipasi Bandung Raya yang merupakan hasil penggabungan dari Persipasi Bekasi dengan Pelita Bandung Raya yang waktu itu berlaga di Liga Super Indonesia. Pemilik Persipasi Bandung Raya, Ari D. Sutedi akhirnya menjual klubnya ke Achsanul Qosasi, dan kemudian bertransformasi menjadi Madura United FC. Kini, Madura United menjelma menjadi salah satu klub terkuat di liga utama Indonesia.</p>
        </div>
    </div>
</div>

@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')