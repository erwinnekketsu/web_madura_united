@include('layouts.head')
@include('layouts.header')
<style>

    @media (max-width: 360px){

        #mobile-nav-toggle {
            display: inline;
            top: 12% !important;
        }
        #logo-header {
            width: 50%;
            padding-left: 4%;
        }
    }
    @media screen and (max-width: 568px) and (min-width: 320px)
    {
        .content-club p {
            width: 100%;
            font-size: 14px;
            color: #000;
        }
    }
</style>
<div class="main-club">
    <div class="club">
        <div class="header-club">
            <div class="header-club-logo">
                <img src="{{url('')}}/assets/img/team/Madura United.png" alt="">
            </div>
            <div class="header-club-title">
                <h1>Hasil Perncarian</h1>
            </div>
        </div>
        <div class="content-club">
            @foreach($data as $item)
            <a href="{{url('/news/detail/')}}/{{$item->slug}}"><h1 style="font-size: x-large;font-weight: bold;">{{$item->title}}</h1>
            <h4 style="font-size: large;margin-bottom:2.5%">{!!$item->summary!!}</h4>
            <h4 style="font-size: small;margin-top:-2.5%;margin-bottom:1%">{{$item->stardate}}</h4>
            </a>
            @endforeach
        </div>
    </div>
</div>

@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')