@include('layouts.head')
@include('layouts.header')
<link rel="stylesheet" href="{{url('')}}/assets/css/klasemenliga.css">
<div class="main-container" id="main-container">
    <div class="next-match-content">
        @foreach($data as $item)
        <div class="next-match-content-left">
            <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="">
            <h3>{{$item->home}}</h3>
        </div>
        <div class="next-match-content-center">
            <h3>NEXT MATCH</h3>
            <h4>Gojek Liga 1, Pertandingan Ke {{$next_match}}</h4>
            <h4 class="next-match-date">{{date('d/m/Y', strtotime( $item->date))}}</h4>
            <h4>{{$item->schedule}} WIB</h4>
        </div>
        <div class="next-match-content-right">
            <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt="">
            <h3>{{$item->away}}</h3>
        </div>
        @php break @endphp
        @endforeach
    </div>
    <div class="klasemen-content">
        <div class="row">
            <div class="col-md-12">
                <!-- Nav tabs -->
                <div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="btn">Jadwal</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="btn">Klasemen</a></li>
                    </ul>
    
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            @php $i = 0 @endphp
                            @foreach($data as $item)
                            @if($i < 3)
                            <div class="next-match">
                                <div class="next-match-dates">
                                    <h4 class="date-match">{{$item->date}}</h4>
                                    <h4>{{$item->schedule}} WIB</h4>
                                </div>
                                <div class="next-match-team">
                                    <div class="next-match-team-home">
                                        <h3>{{$item->home}} <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="" > </h3>
                                    </div>
                                    <div class="next-match-team-vs"><h3>vs</h3></div>
                                    <div class="next-match-team-away">
                                        <h3> <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt=""  > {{$item->away}}</h3>
                                    </div>
                                </div>
                                <div class="next-match-type">
                                    <h4>Head To Head</h4>
                                </div>
                            </div>
                            @endif
                            @php $i++ @endphp
                            @endforeach
                            @foreach($data3[0] as $item)
                            <div class="prev-match">
                                <div class="prev-match-dates">
                                    <h4 class="date-match">
                                        @if(!empty($item->date))
                                        {{$item->date}}
                                        @endif
                                    </h4>
                                    <h4>
                                        @if(!empty($item->schedule))
                                        {{$item->schedule}}
                                        @endif
                                    </h4>
                                </div>
                                <div class="prev-match-team">
                                    <div class="next-match-team-home">
                                        <h3>{{$item->home}} <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="" > </h3>
                                    </div>
                                    <div class="next-match-team-vs"><h3>vs</h3></div>
                                    <div class="next-match-team-away">
                                        <h3> <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt=""  > {{$item->away}}</h3>
                                    </div>
                                </div>
                                <div class="prev-match-type">
                                    <h4>Match Report</h4>
                                </div>
                            </div>
                             @endforeach
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <table class="table table-striped table-hover table-responsive danger">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Team</th>
                                    <th>Main</th>
                                    <th>Menang</th>
                                    <th>Seri</th>
                                    <th>Kalah</th>
                                    <th>Cetak Goal</th>
                                    <th>Kemasukan</th>
                                    <th>+-</th>
                                    <th>PTS</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($data2 as $item)
                                @if($item->tim == "Madura United")
                                <tr style="background-color: #9c080d;color:#fff">
                                    <td>{{$i}}</td>
                                    <td>{{$item->tim}}</td>
                                    <td>{{$item->main}}</td>
                                    <td>{{$item->menang}}</td>
                                    <td>{{$item->seri}}</td>
                                    <td>{{$item->kalah}}</td>
                                    <td>{{$item->menang}}</td>
                                    <td>{{$item->kalah}}</td>
                                    <td>{{$item->menang - $item->kalah}}</td>
                                    <td>{{$item->poin}}</td>
                                </tr>
                                @else
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$item->tim}}</td>
                                    <td>{{$item->main}}</td>
                                    <td>{{$item->menang}}</td>
                                    <td>{{$item->seri}}</td>
                                    <td>{{$item->kalah}}</td>
                                    <td>{{$item->menang}}</td>
                                    <td>{{$item->kalah}}</td>
                                    <td>{{$item->menang - $item->kalah}}</td>
                                    <td>{{$item->poin}}</td>
                                </tr>
                                @endif
                                @php $i++ @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')