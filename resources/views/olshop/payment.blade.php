@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .header-main{
        /* margin-top: 0px !important; */
    }
    .payment_main{
        margin-top: 70px !important;
        margin: auto;
        background-color: #fff;
    }
    .title-text {
        margin-top: 0px;
        margin-bottom: 20px;
    }
    .fa-caret-left{
        color: #888888;
    }
</style>
<div class="payment_main">
    <div class="container">
        <div class="row">
            <nav class="col-md-12"> 
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="#">Informasi Pemesan</a></li>
                    <li class="breadcrumb-item" ><a href="#">Konfirmasi Pesanan</a></li>
                </ol>  
            </nav> <!-- col.// -->
            <main class="col-sm-6">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('confirm') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <fieldset>

                    <!-- Form Name -->
                    <legend>Customer Informations</legend>

                    <!-- Text input-->
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class=" control-label" for="textinput">First Name</label>
                            <input type="text" name="first_name" placeholder="First Name" @if(!empty(Session::get('name'))) value="{{Session::get('name')}}" @endif class="form-control" required >
                        </div>
                        <div class="col-sm-6">
                            <label class=" control-label" for="textinput">Last Name</label>
                            <input type="text" name="last_name" placeholder="Last Name" class="form-control" >
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="ontrol-label" for="textinput">Email</label>
                            <input type="text" name="email" placeholder="Email" class="form-control" @if(!empty(Session::get('user_email'))) value="{{Session::get('user_email')}}" @endif required >
                        </div>
                    </div>

                    <!-- Form Name -->
                    <legend>Shipping Informations</legend>
                    <!-- Text input-->
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="ontrol-label" for="textinput">Address/ Alamat Lengkap</label>
                            <textarea type="text" name="address" placeholder="Address" class="form-control">@if(!empty(Session::get('user_address'))) {{Session::get('user_address')}}  @endif</textarea>
                        </div>
                    </div>

                    <!-- Text input-->
                    <!-- <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="control-label" for="textinput">Province</label>
                            <select name="" id="" class="form-control">
                                <option>Choose</option>
                            </select>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label" for="textinput">City</label>
                            <select name="" id="" class="form-control">
                                <option>Choose</option>
                            </select>
                        </div>
                    </div> -->

                    <!-- Text input-->
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label class="control-label" for="textinput">Phone Number</label>
                            <input type="text" name="phone" placeholder="Phone Number" class="form-control" @if(!empty(Session::get('user_phone'))) value="{{Session::get('user_phone')}}" @endif required >
                        </div>
                    </div>

                    <!-- Form Name -->
                    <!-- <legend>Shipping Method</legend> -->
                    <!-- Text input-->
                    <!-- <div class="form-group row">
                        <div class="col-sm-12">
                             <label class="control-label" for="textinput">Select a Courier</label>
                            <select name="" id="" class="form-control">
                                <option>Choose</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                             <label class="control-label" for="textinput">Type of Service</label>
                            <select name="" id="" class="form-control">
                                <option>Choose</option>
                            </select>
                        </div>
                    </div> -->

                    </fieldset>
                
            </main>
            <aside class="col-sm-6">
                <!-- Form Name -->
                <legend>Purchased</legend>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col"> </th>
                                <th scope="col">Product</th>
                                <th scope="col" class="text-center">Quantity</th>
                                <th scope="col" class="text-right">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($order))
                            @php $sum = 0; @endphp
                            @php $i = 0; @endphp
                            @foreach($order as $item)
                            <tr>
                                <td><img src="{{$item['image']}}" style="width: 100px;"/> </td>
                                <td>{{$item['nama_merch']}}</td>
                                <td>
                                    <select name="jumlah[]"class="form-control">
                                        <option>{{$jumlah[$i]}}</option>
                                    </select> 
                                </td>
                                <td class="text-right">Rp . {{$item['harga'] *  $jumlah[$i]}}</td>
                                <input type="hidden" name="harga[]" value="{{$item['harga'] *  $jumlah[$i]}}" >
                            </tr>
                            @php $sum+= $item['harga'] *  $jumlah[$i]; @endphp
                            @php $i++ @endphp
                            @endforeach
                            @endif
                            <!-- <tr>
                                <td></td>
                                <td></td>
                                <td>Sub-Total</td>
                                <td class="text-right">255,90 €</td>
                            </tr> -->
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Shipping</td>
                                <td class="text-right">Biaya Pengiriman akan dikirim melalui email</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><strong>Total Pembelian</strong></td>
                                <td class="text-right"><strong>Rp . @if(!empty($sum)) {{$sum}} @endif</strong></td>
                                <input type="hidden" name="total" value="@if(!empty($sum)) {{$sum}} @endif" >
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{url('')}}/shop_cart" class="text-left"><i class="fa fa-caret-left"></i>Lanjut Belanja</a>
                    </div>
                    <div class="col-md-6 text-right">
                        @if(!empty($sum)) 
                        <input type="submit" class="btn btn-warning text-right" value="Konfirmasi">
                        @endif
                    </div>
                </div>
                
                
                </form>
            </aside> <!-- col.// -->
        </div>
    </div>
</div>
@include('olshop.layouts.footer')