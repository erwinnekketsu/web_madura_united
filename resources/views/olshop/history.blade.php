@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .header-main{
        /* margin-top: 0px !important; */
    }
    .payment_main{
        margin-top: 70px !important;
        margin: auto;
        background-color: #fff;
        height: 424px;
    }
    .title-text {
        margin-top: 0px;
        margin-bottom: 20px;
    }
    .fa-caret-left{
        color: #888888;
    }
</style>
<div class="payment_main">
    <div class="container">
        <div class="row">
            <main class="col-sm-12" style="padding-top: 1%;padding-bottom:1%;">

                <div class="card">
                    <table class="table table-hover shopping-cart-wrap">
                        <thead class="text-muted">
                            <tr>
                                <th scope="col">Id Transaksi</th>
                                <th scope="col" width="120">Tgl Transaksi</th>
                                <th scope="col" width="120">Total Transaksi</th>
                                <th scope="col" width="120">Status</th>
                                <th scope="col" width="200">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($history))
                            @php $i = 1 @endphp
                            @foreach($history as $item)
                            <tr>
                                <td><a href="{{url('history_order_detail')}}/{{$item->id_transaksi}}" >{{$item->id_transaksi}}</a></td>
                                <td style="color:black;">{{$item->tgl_transaksi}}</td>
                                <td style="color:black;">{{$item->total_transaksi}}</td>
                                <td style="color:black;">@if($item->status == 1) Lunas @else Belum Lunas @endif</td>
                                <td>
                                    @if($item->status == 1)
                                    <button type="button" class="btn btn-danger btn-sm" >Sudah Lunas</button>
                                    @else 
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal{{$i}}">Kirim Bukti Pembayaran</button>
                                    @endif
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal{{$i}}" role="dialog">
                                <div class="modal-dialog">
                                
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Kirim Bukti Pembayaran</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-footer">
                                        <form class="" id="upload_form{{$i}}" role="form" method="POST" action="" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="col-md-8">
                                                <div class="item form-group">
                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="biaya_pengiriman">Bukti Pembayaran <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <input id="bukti_pembayaran" class="form-control col-md-12 col-xs-12" name="bukti_pembayaran" placeholder="Bukti Pembayaran" required="required" type="file">
                                                        <input id="id_transaksi{{$i}}" class="form-control col-md-12 col-xs-12" name="id_transaksi" placeholder="Bukti Pembayaran"  type="hidden" value="{{$item->id_transaksi}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <button id="addbtn{{$i}}" type="button" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                                        </form>
                                    </div>
                                    </div>
                                
                                </div>
                            </div>
                            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                            <script type="text/javascript">
                                $("#addbtn{{$i}}").click(function(){
                                        var token = $('#access_token').val();
                                        var id = $('#id_transaksi{{$i}}').val();
                                        // alert(id);  
                                        $.ajax({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            url:'http://api.maduraunitedfc.com/api/bukti_pembayaran/'+id,
                                            data:new FormData($("#upload_form{{$i}}")[0]),
                                            dataType:'json',
                                            async:false,
                                            type:'post',
                                            processData: false,
                                            contentType: false,
                                            success:function(response){
                                                console.log(response);
                                                if (response != null) {
                                                    window.location.replace("{{ url('') }}/history_order");
                                                } else {
                                                    alert('eyoy');
                                                }
                                            },
                                            beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } 

                                        });
                                    });

                                
                            </script>
                            @php $i++ @endphp
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div> <!-- card.// -->

            </main> <!-- col.// -->
            <!-- col.// -->

        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script type="text/javascript">
          $("#addbtn").click(function(){
          		var token = $('#access_token').val();
                var id = $('#id_transaksi').val();
                alert(id);  
                $.ajax({
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                    url:'http://api.maduraunitedfc.com/api/bukti_pembayaran/'+id,
                    data:new FormData($("#upload_form")[0]),
                    dataType:'json',
                    async:false,
                    type:'post',
                    processData: false,
                    contentType: false,
                    success:function(response){
                        console.log(response);
                          if (response != null) {
                              window.location.replace("{{ url('') }}/history_order");
                          } else {
                              alert('eyoy');
                          }
                    },
				  	beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } 

                });
             });

          
      </script>
@include('olshop.layouts.footer')