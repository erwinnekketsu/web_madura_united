@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .section-content.bg.padding-y{
        margin-top: 70px;
    }
    .section-content.bg.padding-y-sm {
        padding-top: 80px;
    }
    .header-main{
        margin-top: 0px !important;
    }
    .olshop_list{
        width: 100%;
        margin: auto;
		padding: 2% 5%;
    }
</style>
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y-sm">
    <div class="container">
    <nav class="mb-3">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Category name</a></li>
        <li class="breadcrumb-item"><a href="#">Sub category</a></li>
        <li class="breadcrumb-item active" aria-current="page">Items</li>
    </ol> 
    </nav>

    <div class="row">
    <div class="col-xl-12 col-md-9 col-sm-12">


    <main class="card">
        <div class="row no-gutters">
            <aside class="col-sm-6 border-right">
    <article class="gallery-wrap"> 
    <div class="img-big-wrap">
    <div> <a href="{{url('')}}/assets/img/olshop/items/1.png" data-fancybox=""><img src="{{$data->image}}"></a></div>
    </div> <!-- slider-product.// -->
    <div class="img-small-wrap">
    <div class="item-gallery"> <img src="{{$data->image}}"></div>
    <div class="item-gallery"> <img src="{{$data->image}}"></div>
    <div class="item-gallery"> <img src="{{$data->image}}"></div>
    <div class="item-gallery"> <img src="{{$data->image}}"></div>
    </div> <!-- slider-nav.// -->
    </article> <!-- gallery-wrap .end// -->
            </aside>
            <aside class="col-sm-6">
    <article class="card-body">
    <!-- short-info-wrap -->
        <h3 class="title mb-3">Original Version of Some product name</h3>

    <div class="mb-3"> 
        <var class="price h3 text-warning"> 
            <span class="currency">Rp .</span><span class="num">{{$data->harga}}</span>
        </var> 
        <!-- <span>/per kg</span>  -->
    </div> <!-- price-detail-wrap .// -->
    <!-- <dl>
    <dt>Description</dt>
    <dd><p>Here goes description consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco </p></dd>
    </dl> -->
    <!-- <dl class="row">
    <dt class="col-sm-3">Model#</dt>
    <dd class="col-sm-9">12345611</dd>

    <dt class="col-sm-3">Color</dt>
    <dd class="col-sm-9">Black and white </dd>

    <dt class="col-sm-3">Delivery</dt>
    <dd class="col-sm-9">Russia, USA, and Europe </dd>
    </dl> -->
    <div class="rating-wrap">

        <ul class="rating-stars">
            <li style="width:80%" class="stars-active"> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
            </li>
            <li>
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
            </li>
        </ul>
        <div class="label-rating">132 reviews</div>
        <div class="label-rating">154 orders </div>
    </div> <!-- rating-wrap.// -->
    <hr>
        <div class="row">
            <div class="col-sm-5">
                <dl class="dlist-inline">
                <dt>Quantity: </dt>
                <dd> 
                    @if($data->stok > 0)
                    <select class="form-control form-control-sm" style="width:70px;">
                        @for($i=1;$i<=$data->stok;$i++)
                        <option>{{$i}}</option>
                        @endfor
                    </select>
                    @else 
                    Stok Kosong
                    @endif
                </dd>
                </dl>  <!-- item-property .// -->
            </div> <!-- col.// -->
            <div class="col-sm-7">
                <dl class="dlist-inline">
                    <dt>Size: </dt>
                    <dd>
                        <label class="form-check form-check-inline">
                        <input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
                        <span class="form-check-label">SM</span>
                        </label>
                        <label class="form-check form-check-inline">
                        <input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
                        <span class="form-check-label">MD</span>
                        </label>
                        <label class="form-check form-check-inline">
                        <input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
                        <span class="form-check-label">XXL</span>
                        </label>
                    </dd>
                </dl>  <!-- item-property .// -->
            </div> <!-- col.// -->
        </div> <!-- row.// -->
        <hr>
        <a href="{{url('')}}/hubungi" class="btn  btn-warning"> <i class="fa fa-envelope"></i> Contact Supplier </a>
        <a href="{{url('')}}/order?id_merch={{$data->id_merch}}&nama_merch={{$data->nama_merch}}&harga={{$data->harga}}&image={{$data->image}}&stok={{$data->stok}}" class="btn  btn-outline-warning"> Start Order </a>
    <!-- short-info-wrap .// -->
    </article> <!-- card-body.// -->
            </aside> <!-- col.// -->
        </div> <!-- row.// -->
    </main> <!-- card.// -->

    <!-- PRODUCT DETAIL -->
    <article class="card mt-3" style="padding:0% 5%;">
        <h1 class="title-text">Rekomendasi Produk</h1>
        <!-- ============== owl slide items  ============= -->
        <div class="owl-carousel slide-items" data-items="5" data-margin="20" data-dots="true" data-nav="true">
            @foreach($data2 as $item)
            <div class="item-slide">
                <figure class="card card-product">
                    <!-- <span class="badge-new"> NEW </span> -->
                    <a href="{{url('olshop/detail/')}}/{{$item->id_merch}}">
                        <div class="img-wrap"> <img src="{{$item->image}}"> </div>
                    </a>
                    <figcaption class="info-wrap ">
                        <h6 class="title text-truncate"><a href="#">{{$item->nama_merch}}</a></h6>
                        <div class="action-wrap">
                            <a href="{{url('')}}/order?id_merch={{$item->id_merch}}&nama_merch={{$item->nama_merch}}&harga={{$item->harga}}&image={{$item->image}}" class="btn btn-sm float-right" style="background-color: #fc0d1b;color: #fff;"> Order </a>
                            <div class="price-wrap h5">
                                <span class="price-new">Rp. {{$item->harga}}</span>
                            </div> <!-- price-wrap.// -->
                        </div> <!-- action-wrap -->
                    </figcaption>
                </figure> <!-- card // -->
            </div>
            @endforeach
        </div>
        <!-- ============== owl slide items .end // ============= -->
    </article> <!-- card.// -->

    <!-- PRODUCT DETAIL .// -->



    </div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->


@include('olshop.layouts.footer')