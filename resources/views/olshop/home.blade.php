@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .olshop_slider{
        width: 100%;
		margin: auto;
		margin-top: 70px;
    }
	.fs-slides{
		height: 600px;
	}
	.olshop_category{
        width: 100%;
        margin: auto;
		padding: 2% 5%;
    }
	.olshop_list{
        width: 100%;
        margin: auto;
		padding: 2% 5%;
    }
	.olshop_category_left{
        /* width: 50%;
        margin: auto;
		padding: 2% 2%;
		float: left; */
    }
	.olshop_category_left img{
        width: 100%;
    }
	.olshop_category_right{
        /* width: 50%;
        margin: auto;
		padding: 0% 2%;
		float: left; */
    }
	.olshop_category_right_top{
		height: 50%;
    }
	.olshop_category_right_top img{
		width: 100%;
    }
	.olshop_category_right_bottom{
        height: 50%;
    }
	.olshop_category_right_bottom img{
        width: 100%;
    }
</style>
<link rel="stylesheet" href="{{url('')}}/assets/css/main-news.css">
<link rel="stylesheet" href="{{url('')}}/assets/css/slide-main.css">

<div class="olshop_slider">
    <div data-am-fadeshow="next-prev-navigation">

		<!-- Radio -->
		<input type="radio" name="css-fadeshow" id="slide-1" />
		<input type="radio" name="css-fadeshow" id="slide-2" />
		<input type="radio" name="css-fadeshow" id="slide-3" />

		<!-- Slides -->
		<div class="fs-slides">
			<div class="fs-slide" style="background: url({{url('')}}/assets/img/olshop/bg_shop_copy.png);background-repeat: no-repeat;background-size: 100% 100%;"></div>
			<div class="fs-slide" style="background: url({{url('')}}/assets/img/olshop/bg_shop_copy.png);background-repeat: no-repeat;background-size: 100% 100%;"></div>
			<div class="fs-slide" style="background: url({{url('')}}/assets/img/olshop/bg_shop_copy.png);background-repeat: no-repeat;background-size: 100% 100%;"></div>
		</div>

		<!-- Quick Navigation -->
		<div class="fs-quick-nav">
			<label class="fs-quick-btn" for="slide-1"></label>
			<label class="fs-quick-btn" for="slide-2"></label>
			<label class="fs-quick-btn" for="slide-3"></label>
		</div>
		
		<!-- Prev Navigation -->
		<div class="fs-prev-nav">
			<label class="fs-prev-btn" for="slide-1"></label>
			<label class="fs-prev-btn" for="slide-2"></label>
			<label class="fs-prev-btn" for="slide-3"></label>
		</div>
		
		<!-- Next Navigation -->
		<div class="fs-next-nav">
			<label class="fs-next-btn" for="slide-1"></label>
			<label class="fs-next-btn" for="slide-2"></label>
			<label class="fs-next-btn" for="slide-3"></label>
		</div>

	</div>
</div>
<div class="olshop_category row">
	<div class="olshop_category_left col-md-6">
		<img src="{{url('')}}/assets/img/olshop/maxresdefauclt.png" alt="">
		<img src="{{url('')}}/assets/img/olshop/Rectangle 216.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 95%;height: 100%;">
		<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);">
			<h1 style="margin-top: 0; margin-bottom: 0.8vw; font-size: 5vw; font-weight: bold;color:#fff;">Jersey</h1>
		</div>
	</div>
	<div class="olshop_category_right col-md-6">
		<div class="olshop_category_right_top">
			<img src="{{url('')}}/assets/img/olshop/hat_.png" alt="">
			<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -150%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);">
				<h1 style="margin-top: 0; margin-bottom: 0.8vw; font-size: 5vw; font-weight: bold;color:#fff;">ACCESSORIES</h1>
			</div>
		</div>
		<div class="olshop_category_right_bottom">
			<img src="{{url('')}}/assets/img/olshop/en4eegx549-47-0-1--l.png" alt="">
			<img src="{{url('')}}/assets/img/olshop/Rectangle 216.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 0%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 95%;height: 50%;">
			<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 60%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);">
				<h1 style="margin-top: 0; margin-bottom: 0.8vw; font-size: 5vw; font-weight: bold;color:#fff;">APPAREL</h1>
			</div>
		</div>
	</div>
</div>
<div class="olshop_list">
	<h1 class="title-text">TOP SELLERS</h1>
	<!-- ============== owl slide items  ============= -->
	<div class="owl-carousel slide-items" data-items="5" data-margin="20" data-dots="true" data-nav="true">
		@foreach($data as $item)
		<div class="item-slide">
			<figure class="card card-product">
				<!-- <span class="badge-new"> NEW </span> -->
				<a href="{{url('olshop/detail/')}}/{{$item->id_merch}}">
					<div class="img-wrap"> <img src="{{$item->image}}"> </div>
				</a>
				<figcaption class="info-wrap ">
					<h6 class="title text-truncate"><a href="#">{{$item->nama_merch}}</a></h6>
					<div class="action-wrap">
						<a href="{{url('')}}/order?id_merch={{$item->id_merch}}&nama_merch={{$item->nama_merch}}&harga={{$item->harga}}&image={{$item->image}}" class="btn btn-sm float-right" style="background-color: #fc0d1b;color: #fff;"> Order </a>
						<div class="price-wrap h5">
							<span class="price-new">Rp. {{$item->harga}}</span>
						</div> <!-- price-wrap.// -->
					</div> <!-- action-wrap -->
				</figcaption>
			</figure> <!-- card // -->
		</div>
		@endforeach
	</div>
	<!-- ============== owl slide items .end // ============= -->
</div>
@include('olshop.layouts.footer')