@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .section-content.bg.padding-y{
        margin-top: 70px;
    }
    .section-content.bg.padding-y-sm {
        padding-top: 0px;
    }
    .breadcrumb {
        margin-bottom: 0rem;
    }
    .card-body {
        flex: 1 1 auto;
        padding: 0rem;
    }
    .filter-content.collapse.show{
        padding: 0.75rem 1.25rem;
    }
</style>
<section class="section-content bg padding-y">
    <div class="container">

        <div class="row">
            <aside class="col-sm-3">

                <div class="card card-filter">
                    <article class="card-group-item">
                        <header class="card-header">
                            <a class="" aria-expanded="true" href="#" data-toggle="collapse" data-target="#collapse22">
                                <i class="icon-action fa fa-chevron-down"></i>
                                <h6 class="title">By Category</h6>
                            </a>
                        </header>
                        <div style="" class="filter-content collapse show" id="collapse22">
                            <div class="card-body">
                                <!-- <ul class="list-unstyled list-lg">
                                    <li><a href="#">Cras justo odio <span class="float-right badge badge-light round">142</span> </a></li>
                                    <li><a href="#">Dapibus ac facilisis  <span class="float-right badge badge-light round">3</span>  </a></li>
                                    <li><a href="#">Morbi leo risus <span class="float-right badge badge-light round">32</span>  </a></li>
                                    <li><a href="#">Another item <span class="float-right badge badge-light round">12</span>  </a></li>
                                </ul>   -->
                            </div> <!-- card-body.// -->
                        </div> <!-- collapse .// -->
                    </article> <!-- card-group-item.// -->
                </div> <!-- card.// -->


            </aside> <!-- col.// -->
            <main class="col-sm-9">
                <!-- ========================= SECTION CONTENT ========================= -->
                <section class="section-content bg padding-y-sm">
                <div class="container">
                <div class="card">
                    <div class="card-body">
                <div class="row">
                    <nav class="col-md-3"> 
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Jersey</li>
                    </ol>  
                    </nav> <!-- col.// -->
                </div> <!-- row.// -->
               
                    </div> <!-- card-body .// -->
                </div> <!-- card.// -->

                <div class="padding-y-sm">
                <span>3897 results for "Item"</span>	
                </div>

                <div class="row-sm">
                    @foreach($data as $item)
                    <div class="col-md-3 col-sm-6">
                        <figure class="card card-product">
                            <a href="{{url('olshop/detail/')}}/{{$item->id_merch}}">
                                <div class="img-wrap"> <img src="{{$item->image}}"></div>
                            </a>
                            <figcaption class="info-wrap">
                                <a href="{{url('olshop/detail/')}}/{{$item->id_merch}}" class="title">{{$item->nama_merch}}</a>
                                <a href="{{url('')}}/order?id_merch={{$item->id_merch}}&nama_merch={{$item->nama_merch}}&harga={{$item->harga}}&image={{$item->image}}" class="btn btn-sm float-right" style="background-color: #fc0d1b;color: #fff;"> Order </a>
                                <div class="price-wrap">
                                    <a href="{{url('olshop/detail/')}}/{{$item->id_merch}}"><span class="price-new">Rp . {{$item->harga}}</span></a>
                                    <!-- <del class="price-old">$1980</del> -->
                                </div> <!-- price-wrap.// -->
                            </figcaption>
                        </figure> <!-- card // -->
                    </div> <!-- col // -->
                    @endforeach
                </div> <!-- row.// -->


                </div><!-- container // -->
                </section>
                <!-- ========================= SECTION CONTENT .END// ========================= -->

            </main> <!-- col.// -->
        </div>

    </div> <!-- container .//  -->
</section>

@include('olshop.layouts.footer')