@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .header-main{
        /* margin-top: 0px !important; */
    }
    .payment_main{
        margin-top: 70px !important;
        margin: auto;
        background-color: #fff;
        height: 424px;
    }
    .title-text {
        margin-top: 0px;
        margin-bottom: 20px;
    }
    .fa-caret-left{
        color: #888888;
    }
</style>
<div class="payment_main">
    <div class="container">
        <div class="row">
            <main class="col-sm-12" style="padding-top: 1%;padding-bottom:1%;">

                <div class="card">
                    <table class="table table-hover shopping-cart-wrap">
                        <thead class="text-muted">
                            <tr>
                                <th scope="col">Id Transaksi</th>
                                <th scope="col" width="120">Merchandise</th>
                                <th scope="col" width="120">Jumlah</th>
                                <th scope="col" width="120">Total Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($history))
                            @php $i = 1 @endphp
                            @foreach($history as $item)
                            <tr>
                                <td><a href="{{url('history_order')}}" >{{$item->id_transaksi}}</a></td>
                                <td style="color:black;">{{$item->merch}}</td>
                                <td style="color:black;">{{$item->jumlah}}</td>
                                <td style="color:black;">{{$item->harga}}</td>
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div> <!-- card.// -->

            </main> <!-- col.// -->
            <!-- col.// -->

        </div>
    </div>
</div>
@include('olshop.layouts.footer')