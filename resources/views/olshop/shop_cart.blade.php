@include('layouts.head')
@include('olshop.layouts.header')
<style>
    .header-main{
        /* margin-top: 0px !important; */
    }
    .section-content.bg.padding-y{
        margin-top: 70px;
    }
    .olshop_list{
        width: 100%;
        margin: auto;
		padding: 0% 5%;
    }
    .fa-heart{
        color: #28a745;
    }
    .fa-heart:hover{
        color: #fff;
    }
</style>
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y border-top">
    <div class="container">

    <div class="row">
        <main class="col-sm-12">

            <div class="card">
                <table class="table table-hover shopping-cart-wrap">
                    <thead class="text-muted">
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col" width="120">Quantity</th>
                            <th scope="col" width="120">Price</th>
                            <th scope="col" class="text-right" width="200">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{url('')}}/payment" method="GET">
                        @if(!empty($order))
                        @foreach($order as $item)
                        <tr>
                            <td>
                                <figure class="media">
                                    <div class="img-wrap"><img src="{{$item['image']}}" class="img-thumbnail img-sm"></div>
                                    <figcaption class="media-body">
                                        <h6 class="title text-truncate">{{$item['nama_merch']}}</h6>
                                        <!-- <dl class="dlist-inline small">
                                        <dt>Size: </dt>
                                        <dd>XXL</dd>
                                        </dl>
                                        <dl class="dlist-inline small">
                                        <dt>Color: </dt>
                                        <dd>Orange color</dd>
                                        </dl> -->
                                    </figcaption>
                                </figure> 
                            </td>
                            <td> 
                                @if($item['stok'] > 0)
                                <select name="jumlah[]"class="form-control">
                                    @for($i=1;$i<=$item['stok'];$i++)
                                    <option>{{$i}}</option>
                                    @endfor
                                </select> 
                                @else 
                                Stok Kosong
                                @endif
                            </td>
                            <td> 
                                <div class="price-wrap"> 
                                    <var class="price">Rp. {{$item['harga']}}</var> 
                                    <!-- <small class="text-muted">(USD5 each)</small>  -->
                                </div> <!-- price-wrap .// -->
                            </td>
                            <td class="text-right"> 
                            <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> 
                            <a href="{{url('')}}/cancel_order/{{$item['id_merch']}}" class="btn btn-outline-danger"> × Remove</a>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="text-right"> 
                            </td>
                            <td class="text-right"> 
                                <a data-original-title="Save to Wishlist" title="" href="{{url('')}}/olshop/shop" class="btn btn-info" data-toggle="tooltip"> CONTINUE SHOPING </a>
                                <button type="submit" class="btn btn-warning btn-round"> CHECK OUT</button>
                                
                                <!-- {{ csrf_field() }} -->
                            </td>
                        </tr>
                        </form>
                        @endif
                    </tbody>
                </table>
            </div> <!-- card.// -->

        </main> <!-- col.// -->
        <!-- col.// -->

    </div>

    </div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<div class="olshop_list">
	<h1 class="title-text">Rekomendasi Produk</h1>
	<!-- ============== owl slide items  ============= -->
	<div class="owl-carousel slide-items" data-items="5" data-margin="20" data-dots="true" data-nav="true">
		@foreach($data2 as $item)
        <div class="item-slide">
            <figure class="card card-product">
                <!-- <span class="badge-new"> NEW </span> -->
                <a href="{{url('olshop/detail/')}}/{{$item->id_merch}}">
                    <div class="img-wrap"> <img src="{{$item->image}}"> </div>
                </a>
                <figcaption class="info-wrap ">
                    <h6 class="title text-truncate"><a href="#">{{$item->nama_merch}}</a></h6>
                    <div class="action-wrap">
                        <a href="{{url('')}}/order?id_merch={{$item->id_merch}}&nama_merch={{$item->nama_merch}}&harga={{$item->harga}}&image={{$item->image}}&stok={{$item->stok}}" class="btn btn-sm float-right" style="background-color: #fc0d1b;color: #fff;"> Order </a>
                        <div class="price-wrap h5">
                            <span class="price-new">Rp. {{$item->harga}}</span>
                        </div> <!-- price-wrap.// -->
                    </div> <!-- action-wrap -->
                </figcaption>
            </figure> <!-- card // -->
        </div>
        @endforeach
	</div>
	<!-- ============== owl slide items .end // ============= -->
</div>
@include('olshop.layouts.footer')