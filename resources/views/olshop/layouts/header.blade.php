<!-- plugin: slickslider -->
<link href="{{url('')}}/assets/plugins/slickslider/slick.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/assets/plugins/slickslider/slick-theme.css" rel="stylesheet" type="text/css" />


<!-- plugin: owl carousel  -->
<link href="{{url('')}}/assets/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="{{url('')}}/assets/plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<link href="{{url('')}}/assets/css/uikit.css" rel="stylesheet" type="text/css"/>
<!-- <link href="{{url('')}}/assets/css/bootstrap-custom.css" rel="stylesheet" type="text/css"/> -->
<link href="{{url('')}}/assets/css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
<!-- Font awesome 5 -->
<link href="{{url('')}}/assets/fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">
<style>
    @media screen and (min-width: 1440px) {
        .header-main {
            width: 1440px;
            /* border: 1px solid; */
            background: #ab1216;
            height: auto !important;
            margin: auto;
            padding: 1%;
            position: fixed;
            z-index: 2;
            -webkit-transition: height .5s ease-in-out;
            transition: height .5s ease-in-out;
            transition-property: all;
            transition-timing-function: cubic-bezier(0.7, 1, 0.7, 1);
            margin-top: -70px;
        }
        .show .show {
            padding: 15px !important;
            min-width: 219px !important;
            position: absolute;
            transform: unset !important;
            top: auto !important;
            left: -100% !important;
            /* will-change: transform !important; */
        }
    }

    @media screen and (max-width: 1439px) {
        .header-main {
            width: 100%;
            /* border: 1px solid; */
            background: #ab1216;
            height: auto !important;
            margin: auto;
            padding: 1%;
            position: fixed;
            z-index: 2;
            -webkit-transition: height .5s ease-in-out;
            transition: height .5s ease-in-out;
            transition-property: all;
            transition-timing-function: cubic-bezier(0.7, 1, 0.7, 1);
            margin-top: -70px;
        }
        .show .show {
            padding: 15px !important;
            min-width: 219px !important;
            position: absolute;
            transform: unset !important;
            top: auto !important;
            left: -100% !important;
            /* will-change: transform !important; */
        }
    }
    .logo{
        /* width: 70%; */
    }
    .bg2 {
        background-color: #fc0d1b !important;
    }
    .fa {
        color: #fff;
    }
    .fa-chevron-left  {
        color: #88888888;
    }
    .fa-chevron-right  {
        color: #88888888;
    }
    .icontext .text-wrap {
        color: #fff;
    }
    .brand-wrap ul{
        margin: 1% 0% 0% 0%;
        color: #fff;
    }
    .brand-wrap ul li{
        float: left;
        margin-right: 3%;
        color: #fff;
    }
    .brand-wrap ul li a{
        color: #fff;
        font-weight: bold;
    }
    .sticky {
        display: none;
        transition: height .5s ease-in-out;
    }
</style>
<section class="header-main">
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Well done!</strong> {{session('message')}}
        </div>
    @endif
	<div class="container">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="brand-wrap">
                    <a href="{{url('')}}/olshop"><img class="logo" src="{{url('')}}/assets/img/olshop/olshop_logo.png"></a> 
                </div> <!-- brand-wrap.// -->
            </div>
            <div class="col-md-6">
                <form action="#" class="search-wrap">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-append">
                        <button class="btn" type="submit" style="background-color: #fc0d1b;color:#fff">
                            <i class="fa fa-search"></i>
                        </button>
                        </div>
                    </div>
                </form> <!-- search-wrap .end// -->
            </div> <!-- col.// -->
            <div class="col-md-3 col-sm-6">
                <div class="widgets-wrap d-flex justify-content-center">
                    <div class="widget-header">
                        <a href="{{url('')}}/shop_cart" class="icontext">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-shopping-cart"></i></div>
                            <div class="text-wrap">
                                <!-- <a href="{{url('')}}/shop_cart"> -->
                                <small>Shopping Cart</small>
                                <span>@if(!empty(Session::get('order'))) {{count(Session::get('order'))}} @else 0 @endif items</span>
                                <!-- </a> -->
                            </div>
                        </a>
                    </div> <!-- widget .// -->
                    <div class="widget-header dropdown">
                        @if(empty(Session::get('access_token')))
                        <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                            <div class="text-wrap">
                                <span>Login <i class="fa fa-caret-down"></i></span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-center">
                            <form class="px-4 py-3" method="POST" action="{{ url('login_user') }}">
                                @csrf
                                <div class="form-group">
                                <label>Email address</label>
                                <input type="email" class="form-control" name="email" placeholder="email@example.com">
                                </div>
                                <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="{{url('/register')}}">Have account? Sign up</a>
                                <a class="dropdown-item" href="{{ route('password.request') }}">Forgot password?</a>
                        </div> <!--  dropdown-menu .// -->
                        @else
                            <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                                <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                                <div class="text-wrap">
                                    <span>@if(!empty(Session::get('name'))) {{Session::get('name')}}  @endif <i class="fa fa-caret-down"></i></span>
                                </div>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <a class="dropdown-item" href="{{ url('history_order') }}" >
                                    History Order
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </div> <!-- widget  dropdown.// -->
                </div>	<!-- widgets-wrap.// -->	
            </div> <!-- col.// -->
        </div> <!-- row.// -->
    </div> <!-- container.// -->
    <div class="container sticky" id="myHeader">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="brand-wrap">
                    <!-- <img class="logo" src="{{url('')}}/assets/img/olshop/olshop_logo.png"> -->
                    <ul>
                        <li><a href="{{url('')}}olshop/jersey">JERSEY</a></li>
                        <li><a href="{{url('')}}olshop/apparel">APPAREL</a></li>
                        <li><a href="{{url('')}}olshop/accessoris">ACCESSORIS</a></li>
                        <li><a href="{{url('')}}olshop/store-location">STORE LOCATION</a></li>
                    </ul>
                </div> <!-- brand-wrap.// -->
            </div>
        </div> <!-- row.// -->
    </div>
</section>