    <footer class="footer">
        <div class="footer-left">
            <div class="footer-menu">
                <h1>HOME</h1>
            </div>
            <div class="footer-menu">
                <h1>BERITA</h1>
                <h2>Lorem</h2>
            </div>
            <div class="footer-menu">
                <h1>TEAM</h1>
                <h2>Lorem</h2>
            </div>
            <div class="footer-menu">
                <h1>KOMPETISI</h1>
                <h2>Lorem</h2>
            </div>
            <div class="footer-menu">
                <h1>CLUB</h1>
                <h2>Lorem</h2>
            </div>
            <div class="footer-menu">
                <h1>PERTANDINGAN</h1>
                <h2>Lorem</h2>
            </div>
        </div>
        <hr>
        <div class="footer-right">
            <div class="footer-right-top">
                <div class="footer-medsos"><i class="fa fa-facebook"></i></div>
                <div class="footer-medsos"><i class="fa fa-instagram"></i></div>
                <div class="footer-medsos"><i class="fa fa-twitter"></i></div>
                <div class="footer-medsos"><i class="fa fa-youtube"></i></div>
            </div>
            <div class="footer-right-bottom">
                <img src="{{url('')}}/assets/img/UKURAN-LOGO-UaNTUK-WEBSITE.png" alt="">
            </div>
        </div>
    </footer>
    <!-- jQuery -->
    <script src="{{url('')}}/assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <!-- custom javascript -->
    <script src="{{url('')}}/assets/js/script.js" type="text/javascript"></script>

    <!-- Bootstrap4 files-->
    <script src="{{url('')}}/assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/assets/plugins/owlcarousel/owl.carousel.min.js"></script>
    <script src="{{url('')}}/assets/plugins/slickslider/slick.min.js"></script> 

    <!-- plugin: fancybox  -->
    <script src="{{url('')}}/assets/plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="{{url('')}}/assets/plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <script>
        window.onscroll = function() {myFunction()};

        var header = document.getElementById("myHeader");
        var sticky = header.offsetTop;

        function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.remove("sticky");
        } else {
            header.classList.add("sticky");
        }
        }
    </script>
    
  </body>
</html>