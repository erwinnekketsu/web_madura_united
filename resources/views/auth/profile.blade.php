@include('layouts.head')
@include('layouts.header')
<style>
    .main-club{
        /* height: auto !important; */
    }
    .card{
        padding: 1% 12%; 
        background-color:#F7F7F7
    } 
    @media (max-width: 1024px) {
        .card{
            padding: 1% 0% !important; 
            background-color:#F7F7F7
        } 
        .main-club{
            padding: 0% !important;
        }
    }
    @media (max-width: 360px) {
        #mobile-nav-toggle {
            display: inline;
            top: 9% !important;
        }
    }
    .btn i.fa-facebook{
        color: #3B5998;
    }
    .btn i.fa-twitter{
        color: #55ACEE;
    }
    .logo-md{
        width: 50%;
    }
    .card-header{
        text-align: center;
    }
</style>
<div class="main-club">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <img src="{{url('')}}/assets/img/home/LOGO.png" align="center" class="logo-md" />
                    </div>

                    <div class="card-body">

                        <div class=" col-md-9 col-lg-9 "> 
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Name:</td>
                                    <td>{{$profile->name}}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td>{{$profile->user_email}}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{$profile->user_address}}</td>
                                </tr>
                                <tr>
                                    <td>Phone Number</td>
                                    <td>{{$profile->user_phone}}</td>
                                </tr>
                                
                                </tbody>
                            </table>
                            <a href="{{url('/profile_edit')}}" class="btn btn-danger">Edit Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('content.footer')
@include('layouts.footer')