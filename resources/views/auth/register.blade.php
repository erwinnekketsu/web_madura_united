@include('layouts.head')
@include('layouts.header')
<style>
    .main-club{
        height: auto !important;
    }
    .card{
        padding: 1% 12%; 
        background-color:#F7F7F7
    } 
    @media (max-width: 1024px) {
        .card{
            padding: 1% 0% !important; 
            background-color:#F7F7F7
        } 
        .main-club{
            padding: 0% !important;
        }
    }
    @media (max-width: 360px) {
        #mobile-nav-toggle {
            display: inline;
            top: 9% !important;
        }
    }
    .btn i.fa-facebook{
        color: #3B5998;
    }
    .btn i.fa-twitter{
        color: #55ACEE;
    }
</style>
<div class="main-club">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" >
                    <div class="card-header" style="background-color: #F7F7F7">Register New Account</div>

                    <div class="card-body" style="background-color: #F7F7F7">
                        <form method="POST" action="{{ url('/register') }}">
                            @csrf

                            <div class="form-group col-md-12 row">
                                
                                <div class="col-md-6">
                                    <label for="user_first_name" class="text-md-left">Nama Depan</label>
                                    <input id="user_first_name" type="text" class="form-control{{ $errors->has('user_first_name') ? ' is-invalid' : '' }}" name="user_first_name" value="{{ old('user_first_name') }}" required autofocus>

                                    @if ($errors->has('user_first_name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="user_last_name" class="text-md-left">Nama Belakang</label>
                                    <input id="user_last_name" type="text" class="form-control{{ $errors->has('user_last_name') ? ' is-invalid' : '' }}" name="user_last_name" value="{{ old('user_last_name') }}" required autofocus>

                                    @if ($errors->has('user_last_name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="user_email" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-12">
                                    <input id="user_email" type="user_email" class="form-control{{ $errors->has('user_email') ? ' is-invalid' : '' }}" name="user_email" value="{{ old('user_email') }}" required>

                                    @if ($errors->has('user_email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="user_phone" class="col-md-4 col-form-label text-md-left">Phone Number</label>

                                <div class="col-md-12">
                                    <input id="user_phone" type="user_phone" class="form-control{{ $errors->has('user_phone') ? ' is-invalid' : '' }}" name="user_phone" value="{{ old('user_phone') }}" required>

                                    @if ($errors->has('user_phone'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="user_address" class="col-md-4 col-form-label text-md-left">Address</label>

                                <div class="col-md-12">
                                    <textarea id="user_address" type="user_address" class="form-control{{ $errors->has('user_address') ? ' is-invalid' : '' }}" name="user_address" value="{{ old('user_address') }}" required></textarea>

                                    @if ($errors->has('user_address'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                
                                <div class="col-md-6">
                                    <label for="password" class=" col-form-label text-md-right">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="col-md-6">
                                    <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_conf" required>
                                </div>
                            </div>

                            <div class="form-group col-md-12 row checkbox">
                                
                                <div class="col-md-12">
                                    <label>
                                        <input type="checkbox"> Dengan klik daftar, kamu telah menyetujui <a href="#">Aturan Penggunaan</a> dan <a href="#">Kebijakan Privasi</a>
                                    </label>
                                </div>
                            </div>


                            <div class="form-group col-md-12 row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn col-md-12" style="background-color: #fc0d1b;color:#fff">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                
                                <div class="col-md-12">
                                    <hr>
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                
                                <div class="col-md-6">
                                    <a href="#" class="btn col-md-12" style="background-color:#fff;:#000">
                                        <i class="fa fa-facebook"></i>   Sign Up with Facebbok
                                    </a>
                                </div>
                                
                                <div class="col-md-6">
                                    <a href="#" class="btn col-md-12" style="background-color:#fff;:#000">
                                        <i class="fa fa-twitter"></i>   Sign Up with Twitter
                                    </a>
                                </div>
                            </div>

                            <div class="form-group col-md-12 row">
                                <div class="col-md-12">
                                    <h5 style="color:#000;text-align:center;font-size:large">Sudah Punya Akun? <a href="#" class="col-md-12" style="color:#fc0d1b;font-weight:bold" >
                                        Masuk
                                    </a></h5>
                                    
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('content.footer')
@include('layouts.footer')