@include('layouts.head')
@include('layouts.header')
<style>
    .main-club{
        /* height: auto !important; */
    }
    .card{
        padding: 1% 12%; 
        background-color:#F7F7F7
    } 
    @media (max-width: 1024px) {
        .card{
            padding: 1% 0% !important; 
            background-color:#F7F7F7
        } 
        .main-club{
            padding: 0% !important;
        }
    }
    @media (max-width: 360px) {
        #mobile-nav-toggle {
            display: inline;
            top: 9% !important;
        }
    }
    .btn i.fa-facebook{
        color: #3B5998;
    }
    .btn i.fa-twitter{
        color: #55ACEE;
    }
    .logo-md{
        width: 50%;
    }
    .card-header{
        text-align: center;
    }
</style>
<div class="main-club">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <img src="{{url('')}}/assets/img/home/LOGO.png" align="center" class="logo-md" />
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ url('reset_password') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn " style="background-color: #fc0d1b;color:#fff">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('content.footer')
@include('layouts.footer')
