@include('layouts.head')
@include('layouts.header')
<style>
    .content-club img{
        width: 35%;
    }
    @media screen and (max-width: 360px){
        #mobile-nav-toggle {
            display: inline;
            top: 13% !important;
        }
    }
</style>
<div class="main-club">
    <div class="club">
        <div class="content-club">
            <img src="{{url('')}}/assets/img/team/Madura United.png" alt="">
            <h1>{{$error}}</h1>
            <h3>Error API Koneksi</h3>
        </div>
    </div>
</div>

@include('content.footer-sponsor')
@include('content.footer')
@include('layouts.footer')