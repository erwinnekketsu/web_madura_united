<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <link rel="icon" href="{{url('')}}/assets/img/logo.ico" type="image/ico" />
    <title>Madura United FC --BETA</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('')}}/assets/css/linearicons.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/custom.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/bootstrap.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/nice-select.css">					
    <link rel="stylesheet" href="{{url('')}}/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/main.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/map.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/map_10x16.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/map_24x24.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/reset-fonts-grids.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/footer-sponsor.css">
    <link rel="stylesheet" href="{{url('')}}/assets/css/footer.css">
    <link rel="stylesheet" href="{{url('')}}/assets/slick/slick.css">
    <link rel="stylesheet" href="{{url('')}}/assets/slick/slick-theme.css">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WP5HPL4');</script>
    <!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122376985-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'UA-122376985-1');
	</script>

  </head>
  <body style="background: linear-gradient(#E1120D, #710907);
  background-size: 100% 3000px;
  background-repeat: no-repeat;">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WP5HPL4"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <input type="hidden" name="token_type" id="token_type" value="{{Session::get('token_type')}}">
  <input type="hidden" name="access_token" id="access_token" value="{{Session::get('access_token')}}">
