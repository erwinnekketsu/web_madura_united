<div class="main-header">
    <div class="header">
        <div class="container header-top">
            <div class="row">
                <div class="col-6 top-head-left">				  			
                    <ul style="margin-top: 1.5%;">
                        <li ><a href="#" style="font-size:14px">OFFICIAL WEBSITE OF MADURA UNITED FC</a></li>
                    </ul>
                </div>
                <div class="col-6 top-head-right">
                    <ul style="color: #fff;">
                        <li>
                            <form class="navbar-form" role="search" method="POST" action="{{ url('search') }}">
                                @csrf
                                <div class="input-group">
                                    <input type="text" class="form-control input-search" placeholder="search" name="q">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default button-search" type="submit"><i class="fa fa-search fa-searchs"></i></button>
                                    </div>
                                </div>
                            </form>   
                        </li>
                        <li>
                            @if(empty(Session::get('access_token')))
                            <a href="{{url('/register')}}">Register</a> / <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login <b class="caret"></b></a>
                            <ul class="dropdown-menu" style="padding: 15px;min-width: 250px;left: -40px !important;">
                                    <li>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form" role="form" method="POST" action="{{ url('login_user') }}" accept-charset="UTF-8" id="login-nav">
                                                @csrf
                                                <div class="form-group">
                                                <label class="sr-only" for="emailInput">Email address</label>
                                                <input type="email" class="form-control" id="emailInput" name="email" placeholder="Email address" required>
                                                </div>
                                                <div class="form-group">
                                                <label class="sr-only" for="passwordInput">Password</label>
                                                <input type="password" class="form-control" id="passwordInput" name="password" placeholder="Password" required>
                                                </div>
                                                <div class="checkbox">
                                                <label>
                                                <input type="checkbox"> Remember me
                                                </label>
                                                </div>
                                                <br>
                                                <label>
                                                <a class="btn btn-link" href="{{ route('password.request') }}" style="color: blue;">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                                </label>
                                                <div class="form-group">
                                                <button type="submit" class="btn btn-warning btn-block">Sign in</button>
                                                </div>
                                            </form>
                                                <div class="form-group">
                                                    <a href="{{url('/register')}}" class="btn btn-warning btn-block" style="color:#000">Register</a>
                                                </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="divider"></li>
                                </ul>
                            @else
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{url('/profile')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if(!empty(Session::get('name'))) {{Session::get('name')}}  @endif <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ url('history_order') }}">
                                        History
                                    </a>
                                    <a class="dropdown-item" href="{{ url('profile') }}">
                                        profile
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            @endif
                        </li>
                    </ul>
                </div>
                <div class="col-12 navbar-head">
                    <ul style="margin-top: 1%;">
                        <li>
                            <form class="navbar-form2" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control input-search" placeholder="search" name="q">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default button-search" type="submit"><i class="fa fa-search fa-searchs"></i></button>
                                    </div>
                                </div>
                            </form>   
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mid">
        <div class="head-left">
            @if(!empty($banner_kiri))
            <img src="{{$gambar1}}" alt="" title="" />
            @else
            <img src="{{url('')}}/assets/img/home/players.png" alt="" title="" />
            @endif
        </div>
        <div class="head-center"><a href="{{url('')}}"><img src="{{url('')}}/assets/img/home/LOGO.png" alt="" title="" /></a></div>
        <div class="head-right">
            <!-- @if(!empty($iklan_1))
            <img src="{{$gambar2}}" alt="" title="" />
            @else
            <img src="{{url('')}}/assets/img/home/NEW-BANNER-QUICK-CHICKEN-REVISI.png" alt="" title="" />
            @endif
            @if(!empty($iklan_2))
            <img src="{{$gambar3}}" alt="" title="" />
            @else
            <img src="{{url('')}}/assets/img/home/MBaB.png" alt="" title="" />
            @endif -->
        </div>
    </div>
    <div class="header-menu" id="main-header">
        <div class="align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="{{url('')}}"><img src="{{url('')}}/assets/img/LOGO.png" alt="" title="" /></a>
            </div>
            <div id="logo2">
                <a href="{{url('')}}"><img src="{{url('')}}/assets/img/LOGO WHT.png" alt="" title="" /></a>
            </div>
            <nav id="nav-menu-container">
            <ul class="nav-menu nav nav-pills">
                <li class="menu-active active"><a href="{{url('')}}">HOME</a></li>
                <li><a href="{{url('/club')}}">KLUB</a></li>
                <li><a href="{{url('/team')}}">PEMAIN</a></li>
                <li><a href="{{url('/news')}}">BERITA</a></li>
                <li><a href="{{url('/gallery')}}">GALERI</a></li>
                <li><a href="{{url('')}}/klasemen">KOMPETISI</a></li>
                <li><a href="{{url('')}}/hubungi">HUBUNGI</a></li>
                <!-- <li class="shop"><a href="#">SHOP</a></li>			           -->
            </ul>
            </nav>
        </div>
    </div>
</div>