<!-- <link rel="stylesheet" href="{{url('')}}/assets/css/klasemenliga.css"> -->
<div class="klasmen">
    <table class="table" style="height: 90%;">
        <thead>
        <tr>
            <th>#</th>
            <th>Team</th>
            <th>P</th>
            <th>+-</th>
            <th>PTS</th>
        </tr>
        </thead>
        <tbody>
        @php $i = 1 @endphp
        @foreach($data as $item)
        @if($i == 8)
            @php break @endphp
        @endif
        <tr {{ $item->tim === "Madura United" ? 'class=active' : '' }}>
            <td>{{$i}}</td>
            <td>{{$item->tim}}</td>
            <td>{{$item->main}}</td>
            <td>{{$item->menang - $item->kalah}}</td>
            <td>{{$item->poin}}</td>
        </tr>
        @php $i++ @endphp
        @endforeach
        </tbody>
    </table>
    <a href="{{url('')}}/klasemen" class="full-table-link" style="color:#fff">Selengkapnya</a>
</div>