<style>
    @import url('https://fonts.googleapis.com/css?family=Shadows+Into+Light');
    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-list,
    .slick-track {
        height: 100%;
    }
    .slick-prev:before,
    .slick-next:before {
      color: black;
    }
</style>
<div class="content-many">
    <div class="content-content">
        <div class="content-sponsor">
            @if(!empty($gambar4))
            <img src="{{$gambar4}}" alt="" title="" />
            @else
            <img src="{{url('')}}/assets/img/sponsor.png" alt="">
            @endif
        </div>
        <div class="content-news">
            <div class="news-right">
                <div class="news-right-header">
                    <div class="news-right-left">
                        <h3>Berita Terbaru</h3>
                        <hr>
                    </div>
                    <div class="news-right-right">
                        
                    </div>
                    <hr>
                </div>
                <div class="news-right-content">
                    @php $i = 0 @endphp
                    @foreach($data3 as $item)
                    <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                        <div class="row news-right-content-row" >
                            <div class="col-sm-12">
                                <div class="col-item">
                                    <div class="photo">
                                        <img src="{{$item->image_show_fp}}" class="img-responsive" alt="a" />
                                    </div>
                                    <div class="info">
                                        <div class="row">
                                            <div class="price col-md-12">
                                                <h5>{{$item->stardate}}</h5>
                                            </div>
                                            <div class="title col-md-12">
                                                <h5>{{$item->title}}</h5>
                                            </div>
                                            <div class="resume col-md-12">
                                                {!!$item->summary!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @if($i == 3) @php break; @endphp @endif
                    @php $i++; @endphp
                    @endforeach
                </div>
                <div class="news-right-footer">
                    <h5><a href="{{url('')}}/news">Baca Berita Lainnya</a></h5>
                </div>
            </div>
            <div class="news-left">
                <div class="news-top">
                    <div class="berita-populer">
                        <div class="news-left-header">
                            <div class="news-left-left">
                                <h3>Berita Terpopuler</h3>
                                <hr>
                            </div>
                            <div class="news-left-right">
                                
                            </div>
                            <hr>
                        </div>
                        <div class="news-left-content">
                            @php $i = 0 @endphp
                            @foreach($data6 as $item)
                            <a href="{{url('')}}/news/detail/{{$item->slug}}"  >
                            <div class="row news-left-content-row" >
                                <div class="col-sm-12">
                                    <div class="photo">
                                        <img src="{{$item->image_show_fp}}" class="img-responsive" alt="a" />
                                    </div>
                                    <div class="info">
                                        <div class="row">
                                            <div class="price col-md-12">
                                                <h5>{{$item->stardate}}</h5>
                                            </div>
                                            <div class="title col-md-12">
                                                <h5>{{$item->title}}</h5>
                                            </div>
                                            <div class="resume col-md-12">
                                                {!!$item->summary!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                            @if($i == 2) @php break; @endphp @endif
                            @php $i++; @endphp
                            @endforeach
                        </div>
                        <div class="news-left-footer">
                            <h5><a href="{{url('')}}/news">Baca Berita Lainnya</a></h5>
                        </div>
                    </div>
                </div>   
                <div class="news-bottom">
                    <div class="news-bottom-content" style="margin-top: 0%;height: 403px;" >
                            <a class="twitter-timeline"  height="100%" data-dnt="true" href="https://twitter.com/AchsanulQosasi"
                            data-widget-id="256230760145616896" data-theme="light" data-link-color="#cc0000"
                            data-related="twitterapi,twitter" data-aria-polite="assertive" width="300"
                            height="100%" lang="EN">Tweets oleh @AchsanulQosasi</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </div>
                </div> 
            </div>
        </div>
        <div class="content-shop">
            <div class="content-shop-inner">
                <div class="content-shop-header">
                    <div class="content-shop-header-left">
                        <h3>Shop</h3>
                        <hr>
                    </div>
                    <div class="content-shop-header-right">
                        
                    </div>
                    <hr>
                </div>
                <div class="content-shop-content variable slider">
                    @foreach($data8 as $item)
                    <div class="content-item">
                        <div class="item-photo">
                            <img src="{{$item->image}}" alt="">
                        </div>
                        <div class="item-info">
                            <div class="item-info-name">
                                <h4>{{$item->nama_merch}}</h4>
                                <a href="{{url('')}}/hubungi"><h5>Hubungi Kami</h5></a>  
                            </div>
                            <div class="item-info-button">

                                <!-- <button>BELI</button> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<!-- Swiper JS -->
<script src="{{url('')}}/assets/slick/slick.js"></script>
<script type="text/javascript">
    $(document).on('ready', function() {
      $(".variable").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
        {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
        },
        {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
        },
        {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
      });
    });
</script>

  <script>
    $(document).ready(function() {
        var isshow = localStorage.getItem('isshow');
        if (isshow== null) {
            localStorage.setItem('isshow', 1);

        // alert(isshow);
            // Show popup here
            //$('#myModal').modal('show');
        }
        if (isshow== 1) {
            localStorage.setItem('isshow', 1);

        // alert(isshow);
            // Show popup here
           // $('#myModal').modal('show');
        }
    });
  </script>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <img src="{{$gambar2}}" alt="" width="100%">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
