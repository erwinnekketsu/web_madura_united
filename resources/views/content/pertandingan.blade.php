@include('layouts.css-slider')
<style>
    .swiper-button-prev, .swiper-container-rtl .swiper-button-next{
        left: 0px !important;
    }
    .swiper-button-next, .swiper-container-rtl .swiper-button-prev{
        right: 0px !important;
    }
    .swiper-button-prev, .swiper-button-next {
        position: absolute;
        top: 50%;
        width: 28px;
        height: 20px;
        margin-top: -22px;
        z-index: 10;
        cursor: pointer;
        -moz-background-size: 27px 44px;
        -webkit-background-size: 27px 44px;
        background-size: 27px 44px;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>
<div class="match-content">
    <div class="match-last">
        @php $i=0 @endphp
        @foreach($hasil as $item)
        <div class="match-last-team">
            <img src="{{$item['tim']}}" alt="">
        </div>
        @php $i++ @endphp
        @endforeach
    </div>
    <div class="match-this">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @php $i = 0 @endphp
                @foreach($data4 as $item)
                <div class="swiper-slide">
                    <div class="next-match-content-left">
                        <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="">
                        <h3>{{$item->home}}</h3>
                    </div>
                    <div class="next-match-content-center">
                        <h3>NEXT MATCH</h3>
                        <h4>Gojek Liga 1, Pertandingan Ke {{$next_match + $i}}</h4>
                        <h4 class="next-match-date">{{date('d/m/Y', strtotime( $item->date))}}</h4>
                        <h4>{{$item->schedule}} WIB</h4>
                    </div>
                    <div class="next-match-content-right">
                        <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt="">
                        <h3>{{$item->away}}</h3>
                    </div>
                </div>
                @php $i++ @endphp
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
        <!-- @foreach($data4 as $item)
        <div class="next-match-content-left">
            <img src="{{url('')}}/assets/img/team/{{$item->home}}.png" alt="">
            <h3>{{$item->home}}</h3>
        </div>
        <div class="next-match-content-center">
            <h3>NEXT MATCH</h3>
            <h4>Gojek Liga 1, Pertandingan Ke {{$next_match}}</h4>
            <h4 class="next-match-date">{{$item->date}}</h4>
            <h4>{{$item->schedule}} WIB</h4>
        </div>
        <div class="next-match-content-right">
            <img src="{{url('')}}/assets/img/team/{{$item->away}}.png" alt="">
            <h3>{{$item->away}}</h3>
        </div>
        @php break @endphp
        @endforeach -->
    </div>
    <div class="match-soon">
        @foreach($jadwal as $item)
        <div class="match-soon-team">
            <img src="{{$item['tim']}}" alt="">
        </div>
        @endforeach
    </div>
</div>
@include('layouts.js-slider')