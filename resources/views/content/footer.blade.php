<footer class="footer">
    <div class="footer-left">
        <div class="footer-menu">
            <a href="{{url('')}}"><h1>Home</h1></a>
        </div>
        <div class="footer-menu">
            <a href="{{url('')}}/club"><h1>Klub</h1></a>
            <a href="{{url('')}}/club"><h2>Profile</h2></a>
        </div>
        <div class="footer-menu">
            <a href="{{url('')}}/team"><h1>Pemain</h1></a>
            <a href="{{url('')}}/team"><h2>Senior</h2></a>
            <a href="{{url('')}}/team"><h2>U-19</h2></a>
            <a href="{{url('')}}/team"><h2>Akademi</h2></a>
            <a href="{{url('')}}/team"><h2>Official Staff</h2></a>
        </div>
        <div class="footer-menu">
            <a href="{{url('')}}/news"><h1>Berita</h1></a>
            <a href="{{url('')}}/news"><h2>Baru</h2></a>
            <a href="{{url('')}}/news"><h2>Popular</h2></a>
        </div>
        <div class="footer-menu">
            <a href="{{url('')}}/gallery"><h1>Galeri</h1></a>
            <a href="{{url('')}}/gallery"><h2>Foto</h2></a>
            <a href="{{url('')}}/gallery"><h2>Video</h2></a>
        </div>
        <div class="footer-menu">
            <a href="{{url('')}}/klasemen"><h1>Pertandingan</h1></a>
            <a href="{{url('')}}/klasemen"><h2>Jadwal</h2></a>
            <a href="{{url('')}}/klasemen"><h2>Klasemen</h2></a>
        </div>
    </div>
    <hr>
    <div class="footer-right">
        <div class="footer-right-top">
            <a href="#" class="footer-medsos"><i class="fa fa-facebook"></i></a>
            <a href="#" class="footer-medsos"><i class="fa fa-instagram"></i></a>
            <a href="#" class="footer-medsos"><i class="fa fa-twitter"></i></a>
            <a href="#" class="footer-medsos"><i class="fa fa-youtube"></i></a>
        </div>
        <div class="footer-right-bottom">
            <img src="{{url('')}}/assets/img/LOGO WHT.png" alt="">
        </div>
    </div>
</footer>
<div class="footer-title">
    <div class="container footer-title-top">
        <div class="row">
            <div class="col-6 top-head-left">				  			
                
                <a href="#" style="font-size:14px;color: #868686;padding-top:8px;float:left;">© 2018 MADURA UNITED. ALL RIGHTS RESERVED.</a>
            </div>
            <div class="col-6 top-head-right" >
                <h1 style="margin-top: 7px;margin-bottom: 1.5%;color:#868686;font-size:14px;">TERMS OF USE     |      PRIVACY POLICY</h1>
            </div>
            <div class="col-12 navbar-head">
                
            </div>
        </div>
    </div>
</div>
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("main-header");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
