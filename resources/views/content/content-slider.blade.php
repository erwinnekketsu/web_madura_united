<div class="content">
    <div class="right-content">
        <div id="demo" class="carousel slide" data-ride="x">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                @if(!empty($data7[0]))
                    @php $jml = count($data7) + 2 @endphp
                    @for($a=0;$a<=$jml;$a++)
                    <li data-target="#demo" data-slide-to="{{$a}}" @if($a==0) class="active" @endif></li>
                    @endfor
                @else
                    @php $jml = 3 @endphp
                    @for($a=0;$a<$jml;$a++)
                    <li data-target="#demo" data-slide-to="{{$a}}" @if($a==0) class="active" @endif></li>
                    @endfor
                @endif
            </ul>
            
            <!-- The slideshow -->
            <div class="carousel-inner">
                @if(!empty($data7[0]))
                    @php $i = 0 @endphp
                    @foreach($data7 as $item)
                    <div class="carousel-item @if($i == 0) active @endif">
                        <a href="#" >
                            <img src="{{$item->link}}" alt="New York" width="100%" height="100%">
                            <!-- <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: 100%;height: 100%;"> -->
                        
                        </a>
                    </div>
                    @php $i++; @endphp
                    @endforeach
                @endif

                @if(!empty($data7[0]))
                    @php $i = 1 @endphp
                @else
                    @php $i = 0 @endphp
                @endif
                @foreach($data3 as $item)
                <div class="carousel-item @if($i == 0) active @endif">
                    <a href="{{url('')}}/news/detail/{{$item->slug}}" >
                        <img src="{{$item->image_show_fp}}" alt="New York" width="100%" height="100%">
                        <img src="{{url('')}}/assets/img/olshop/Rectangle 48.png" alt="" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-family: sans-serif; text-align: center; text-shadow: 0 0 20px rgba(0,0,0,0.5);width: auto;height: 100%;">
                        <div class="slider-text">
                            <h5>Berita</h5>
                            <h1>{{$item->title}}</h1>
                        </div>
                    </a>
                </div>
                @if($i == 3) @php break; @endphp @endif
                @php $i++; @endphp
                @endforeach
            </div>
            
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </div>
    <div class="left-content">
        <div class="head-left-content">
            <div class="head-left-content-img">
                <img src="{{url('')}}/assets/img/GOJEK_Liga_1_logo.png" alt="New York" >
            </div>
            <div class="head-left-content-title">
                <h4>Klasemen Gojek Liga 1</h4>
            </div>
        </div>
        <hr>
        <div class="classement-left-content">
            @include('content.klasemen')
        </div>
    </div>
</div>